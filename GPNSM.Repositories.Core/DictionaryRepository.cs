﻿using GPNSM.Contracts.Data.Entities;
using GPNSM.Data.Core.Contracts;
using GPNSM.Repositories.Core.Contracts;

namespace GPNSM.Repositories.Core
{
    public abstract class DictionaryRepository<TEntity> : BaseRepository<TEntity, int>, IDictionaryRepository<TEntity>
        where TEntity : class, IDictionaryEntity
    {
        public DictionaryRepository(IDbContext context) : base(context)
        {
        }
    }
}
