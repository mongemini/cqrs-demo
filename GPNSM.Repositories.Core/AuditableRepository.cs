﻿using GPNSM.Contracts.Data.Entities;
using GPNSM.Data.Core.Contracts;
using GPNSM.Repositories.Core.Contracts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Repositories.Core
{
    public abstract class AuditableRepository<TEntity, TKey> : BaseRepository<TEntity, TKey>
        where TEntity : class, IAuditableEntity<TKey>
    {
        public AuditableRepository(IDbContext context) : base(context) { }
        public override void Add(TEntity entity)
        {
            entity.UpdatedDate = entity.CreatedDate = DateTime.UtcNow;
            base.Add(entity);
        }

        public override Task AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            entity.UpdatedDate = entity.CreatedDate = DateTime.UtcNow;
            return base.AddAsync(entity, cancellationToken);
        }

        public override void Update(TEntity entity)
        {
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }
    }

    public abstract class AuditableRepository<TEntity> : Repository<TEntity>, IRepository<TEntity>
        where TEntity : class, IAuditableEntity
    {
        public AuditableRepository(IDbContext context) : base(context)
        {
        }

        public override void Update(TEntity entity)
        {
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }
    }
}
