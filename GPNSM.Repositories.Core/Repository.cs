﻿using GPNSM.Contracts.Data.Entities;
using GPNSM.Data.Core.Contracts;
using GPNSM.Repositories.Core.Contracts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Repositories.Core
{
    public abstract class Repository<TEntity> : BaseRepository<TEntity, Guid>, IRepository<TEntity> where TEntity : class, IEntity
    {
        public Repository(IDbContext context) : base(context) { }
        public override void Add(TEntity entity)
        {
            entity.CreatedDate = DateTime.UtcNow;
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();
            base.Add(entity);
        }

        public override Task AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            entity.CreatedDate = DateTime.UtcNow;
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();
            return base.AddAsync(entity, cancellationToken);
        }
    }
}
