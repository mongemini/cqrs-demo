﻿using GPNSM.Contracts.Data.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Repositories.Core.Contracts
{
    public interface IArchivableRepository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : class, IArchivableEntity<TKey>
    {
        void Recover(TKey id);
        Task RecoverAsync(TKey id, CancellationToken cancellationToken);
        IQueryable<TEntity> GetAllWithDeleted();
        Task PermanentRemoveRangeAsync(Expression<Func<TEntity, bool>> criteria, CancellationToken cancellationToken);
    }

    public interface IArchivableRepository<TEntity> : IArchivableRepository<TEntity, Guid> where TEntity : class, IArchivableEntity
    {
    }
}
