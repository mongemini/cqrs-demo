﻿using GPNSM.Contracts.Data.Entities;

namespace GPNSM.Repositories.Core.Contracts
{
    public interface IDictionaryRepository<TEntity> : IRepository<TEntity, int>
        where TEntity : class, IDictionaryEntity
    {
    }
}
