﻿using GPNSM.Contracts.Data.Entities;
using GPNSM.Data.Core.Contracts;
using GPNSM.Repositories.Core.Contracts;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace GPNSM.Repositories.Core
{
    public abstract class ArchivableRepository<TEntity, TKey> : AuditableRepository<TEntity, TKey>, IArchivableRepository<TEntity, TKey>
        where TEntity : class, IArchivableEntity<TKey>
    {
        public ArchivableRepository(IDbContext context) : base(context)
        {
        }

        public override IQueryable<TEntity> GetAll()
        {
            return base.GetAll().Where(a => a.IsDeleted == false);
        }

        public IQueryable<TEntity> GetAllWithDeleted()
        {
            return base.GetAll();
        }

        public override Task RemoveRangeAsync(Expression<Func<TEntity, bool>> criteria, CancellationToken cancellationToken)
        {
            return GetAll().Where(criteria).UpdateAsync(GetInstance(), cancellationToken);
        }

        public Task PermanentRemoveRangeAsync(Expression<Func<TEntity, bool>> criteria, CancellationToken cancellationToken)
        {
            return GetAllWithDeleted().Where(criteria).DeleteAsync(cancellationToken);
        }

        public override void Delete(TEntity entity)
        {
            entity.IsDeleted = true;
            Update(entity);
        }

        public void Recover(TKey id)
        {
            var entity = Set.Find(new object[] { id });
            if (entity == null) return;
            entity.IsDeleted = false;
            Update(entity);
        }

        public async Task RecoverAsync(TKey id, CancellationToken cancellationToken)
        {
            var entity = await Set.FindAsync(new object[] { id }, cancellationToken);
            if (entity == null) return;
            entity.IsDeleted = false;
            Update(entity);
        }

        public static Expression<Func<TEntity, TEntity>> GetInstance()
        {
            var t = typeof(TEntity);
            var val = Expression.Constant(true);
            var instance = Expression.Parameter(t);
            var newExp = Expression.New(t);
            MemberInfo item1Member = t.GetMember("IsDeleted")[0];
            MemberBinding item1MemberBinding =
            Expression.Bind(item1Member, val);
            MemberInitExpression memberInitExpression = Expression.MemberInit(newExp, item1MemberBinding);
            var lambda = Expression.Lambda<Func<TEntity, TEntity>>(memberInitExpression, instance);
            return lambda;
        }

        public override Task ClearAsync(CancellationToken cancellationToken)
        {
            return GetAllWithDeleted().DeleteAsync(cancellationToken);
        }
    }

    public abstract class ArchivableRepository<TEntity> : ArchivableRepository<TEntity, Guid>, IArchivableRepository<TEntity>
        where TEntity : class, IArchivableEntity
    {
        public ArchivableRepository(IDbContext context) : base(context)
        {
        }
    }
}
