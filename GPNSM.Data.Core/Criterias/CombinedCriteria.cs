﻿using GPNSM.Common.Core.Extensions;
using GPNSM.Data.Core.Contracts.Criterias;
using System;
using System.Linq.Expressions;

namespace GPNSM.Data.Core.Criterias
{
    public class CombinedCriteria<TEntity> : ICriteria<TEntity> where TEntity : class
    {
        public CombinedCriteria(ICriteria<TEntity> from, ICriteria<TEntity> to)
        {
            _from = from;
            _to = to;
        }
        private readonly ICriteria<TEntity> _from;
        private readonly ICriteria<TEntity> _to;

        public Expression<Func<TEntity, bool>> Build()
        {
            return _from.Build().And(_to.Build());
        }
    }
}
