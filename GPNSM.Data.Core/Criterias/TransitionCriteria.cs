﻿using GPNSM.Common.Core.Extensions;
using GPNSM.Data.Core.Contracts.Criterias;
using GPNSM.Data.Core.Criterias.Helpers;
using System;
using System.Linq.Expressions;

namespace GPNSM.Data.Core.Criterias
{
    public class TransitionCriteria<TFrom, TTo> : ICriteria<TTo>
        where TTo : class
        where TFrom : class
    {
        public TransitionCriteria(ICriteria<TFrom> criteria, Expression<Func<TTo, TFrom>> selector)
        {
            _source = criteria;
            _selector = selector;
        }
        private readonly ICriteria<TFrom> _source;
        private readonly Expression<Func<TTo, TFrom>> _selector;

        public Expression<Func<TTo, bool>> Build()
        {
            return _source.Build().Convert(_selector);
        }
    }
}
