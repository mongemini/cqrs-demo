﻿using System;
using System.Linq.Expressions;

namespace GPNSM.Data.Core.Criterias
{
    public class ExpressionCriteria<TEntity> : CriteriaBase<TEntity> where TEntity : class
    {
        public ExpressionCriteria(Expression<Func<TEntity, bool>> expression)
        {
            _expression = expression;
        }
        private readonly Expression<Func<TEntity, bool>> _expression;

        public override Expression<Func<TEntity, bool>> Build()
        {
            return _expression;
        }
    }
}
