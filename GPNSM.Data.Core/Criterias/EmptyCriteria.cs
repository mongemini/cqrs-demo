﻿using GPNSM.Data.Core.Contracts.Criterias;
using System;
using System.Linq.Expressions;

namespace GPNSM.Data.Core.Criterias
{
    public class EmptyCriteria<TEntity> : ICriteria<TEntity> where TEntity : class
    {
        public Expression<Func<TEntity, bool>> Build()
        {
            return a => true;
        }
    }
}
