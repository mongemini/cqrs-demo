﻿using GPNSM.Data.Core.Contracts.Criterias;
using System;
using System.Linq.Expressions;

namespace GPNSM.Data.Core.Criterias
{
    public abstract class CriteriaBase<TEntity> : ICriteria<TEntity>
        where TEntity : class
    {
        public abstract Expression<Func<TEntity, bool>> Build();
        public ICriteria<TEntity> True => new EmptyCriteria<TEntity>();
    }
}
