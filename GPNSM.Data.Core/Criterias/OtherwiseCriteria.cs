﻿using GPNSM.Common.Core.Extensions;
using GPNSM.Data.Core.Contracts.Criterias;
using GPNSM.Data.Core.Criterias.Helpers;
using System;
using System.Linq.Expressions;

namespace GPNSM.Data.Core.Criterias
{
    public class OtherwiseCriteria<TEntity> : ICriteria<TEntity> where TEntity : class
    {
        public OtherwiseCriteria(ICriteria<TEntity> left, ICriteria<TEntity> right)
        {
            _left = left;
            _right = right;
        }
        private readonly ICriteria<TEntity> _left;
        private readonly ICriteria<TEntity> _right;

        public Expression<Func<TEntity, bool>> Build()
        {
            return _left.Build().Or(_right.Build());
        }
    }
}
