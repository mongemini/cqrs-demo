﻿using GPNSM.Contracts.Data.Entities;
using System;

namespace GPNSM.Data.Core.Entities
{
    public class ArchivableEntity<TKey> : AuditableEntity<TKey>, IArchivableEntity<TKey>
    {
        public bool IsDeleted { get; set; }
    }

    public class ArchivableEntity : ArchivableEntity<Guid>, IArchivableEntity { }
}
