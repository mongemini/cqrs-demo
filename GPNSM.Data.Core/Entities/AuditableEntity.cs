﻿using GPNSM.Contracts.Data.Entities;
using System;

namespace GPNSM.Data.Core.Entities
{
    public abstract class AuditableEntity<TKey> : BaseEntity<TKey>, IAuditableEntity<TKey>
    {
        public DateTime CreatedDate { get; set; }
        public TKey CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public  TKey UpdatedBy { get; set; }
    }

    public abstract class AuditableEntity : AuditableEntity<Guid>, IAuditableEntity { }
}
