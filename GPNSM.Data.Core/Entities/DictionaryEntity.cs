﻿using GPNSM.Contracts.Data.Entities;

namespace GPNSM.Data.Core.Entities
{
    public class DictionaryEntity : BaseEntity<int>, IDictionaryEntity
    {
        public string Value { get; set; }
    }
}
