﻿namespace GPNSM.Data.Core.Contracts.Criterias
{
    public interface IPagedCriteria : ISortCriteria, IPageOptions
    {
    }
}
