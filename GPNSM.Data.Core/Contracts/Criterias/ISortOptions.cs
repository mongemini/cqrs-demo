﻿namespace GPNSM.Data.Core.Contracts.Criterias
{
    public interface ISortOptions
    {
        string Sort { get; }
        int Direction { get; }
    }
}
