﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Data.Core.Contracts.Criterias
{
    public interface IPageOptions
    {
        int? Size { get; }
        int? Page { get; }
    }
}
