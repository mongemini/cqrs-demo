﻿using System;
using System.Linq.Expressions;

namespace GPNSM.Data.Core.Contracts.Criterias
{
    public interface ICriteria<TEntity> where TEntity : class
    {
        Expression<Func<TEntity, bool>> Build();
    }
}
