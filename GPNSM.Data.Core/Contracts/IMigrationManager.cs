﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Data.Core.Contracts
{
    public interface IMigrationManager
    {
        Task MigrateAsync(CancellationToken cancellationToken);
        Task<IDictionary<string, string[]>> GetPendingMigrationsAsync(CancellationToken cancellationToken);
    }
}
