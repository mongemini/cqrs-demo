﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Data.Core.Contracts
{
    public interface IDatabaseFacadeProvider
    {
        DatabaseFacade Database { get; }
    }
}
