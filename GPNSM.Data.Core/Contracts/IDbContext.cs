﻿using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Data.Core.Contracts
{
    public interface IDbContext : IDisposable
    {
        Task MigrateAsync(CancellationToken cancellationToken);
        string ContextName { get; }
        Task<string[]> GetMigrationsAsync(CancellationToken cancellationToken);
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        IModel Model { get; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        int SaveChanges();
        string ConnectionString { get; }
        string Id { get; }
    }
}
