﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Data.Core.Contracts
{
    public interface IDbSet<TEntity> : IQueryable<TEntity>, IEnumerable<TEntity>, IEnumerable, IQueryable
        where TEntity : class
    {
        EntityEntry Attach(TEntity entity);
        void Add(TEntity entity);
        void Remove(TEntity entity);
        Task AddAsync(TEntity entity, CancellationToken cancellationToken);
        TEntity Find(object[] keyValues);
        Task<TEntity> FindAsync(object[] keyValues, CancellationToken cancellationToken);
    }
}
