﻿using GPNSM.Data.Core.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Data.Core
{
    public class DbQuery<TEntity> : IDbSet<TEntity> where TEntity : class
    {
        public DbQuery(DbSet<TEntity> set)
        {
            _set = set;
            _query = set;
        }
        private readonly DbSet<TEntity> _set;

        public EntityEntry Attach(TEntity entity)
        {
            return _set.Attach(entity);
        }

        public void Add(TEntity entity)
        {
            _set.Add(entity);
        }

        public void Remove(TEntity entity)
        {
            _set.Remove(entity);
        }

        public Task AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            return _set.AddAsync(entity).AsTask();
        }

        public TEntity Find(object[] keyValues)
        {
            return _set.Find(keyValues);
        }

        public Task<TEntity> FindAsync(object[] keyValues, CancellationToken cancellationToken)
        {
            return _set.FindAsync(keyValues, cancellationToken).AsTask();
        }

        #region IQueryable
        private readonly IQueryable<TEntity> _query;

        public Type ElementType => _query.ElementType;

        public Expression Expression => _query.Expression;

        public IQueryProvider Provider => _query.Provider;

        public IEnumerator<TEntity> GetEnumerator()
        {
            return _query.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
