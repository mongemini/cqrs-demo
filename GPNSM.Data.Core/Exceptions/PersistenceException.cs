﻿using GPNSM.Common.Core.Exceptions;
using System;

namespace GPNSM.Data.Core.Exceptions
{
    public class PersistenceException : BaseException
    {
        public PersistenceException() : base() { }
        public PersistenceException(string message) : base(message) { }
        public PersistenceException(string message, Exception innerException) : base(message, innerException) { }
        protected PersistenceException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
