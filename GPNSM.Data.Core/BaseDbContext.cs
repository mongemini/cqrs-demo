﻿using GPNSM.Data.Core.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Data.Core
{
    [DebuggerDisplay("{ContextName}:{Id}")]
    public abstract class BaseDbContext : DbContext, IDbContext, IDatabaseFacadeProvider
    {
        protected BaseDbContext(DbContextOptions options) : base(options)
        {
            //var sqlServerOptionsExtension = options.Extensions.First() as SqlServerOptionsExtension;
            //if (sqlServerOptionsExtension != null)
            //    ConnectionString = sqlServerOptionsExtension.ConnectionString;
            Id = Guid.NewGuid().ToString();
        }


        IDbSet<TEntity> IDbContext.Set<TEntity>()
        {
            return new DbQuery<TEntity>(Set<TEntity>());
        }

        public string Id { get; private set; }
        private bool _isDisposed;

        public string ConnectionString { get; }

        public string ContextName => GetType().Name;

        public override void Dispose()
        {
            if (_isDisposed) return;
            base.Dispose();
            _isDisposed = true;
        }

        private void DetachAll()
        {
            var entires = ChangeTracker.Entries().ToArray();
            foreach (var entry in entires)
            {
                if (entry.Entity != null)
                    entry.State = EntityState.Detached;
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            var result = await base.SaveChangesAsync(cancellationToken);
            DetachAll();
            return result;
        }

        public override int SaveChanges()
        {
            var result = base.SaveChanges();
            DetachAll();
            return result;
        }

        public int SaveChangesWithoutDetach()
        {
            return base.SaveChanges();
        }

        public Task<int> SaveChangesWithoutDetachAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SaveChangesAsync(cancellationToken);
        }

        public Task MigrateAsync(CancellationToken cancellationToken)
        {
            return Database.MigrateAsync(cancellationToken);
        }

        public async Task<string[]> GetMigrationsAsync(CancellationToken cancellationToken)
        {
            var migrations = await Database.GetPendingMigrationsAsync(cancellationToken);
            return migrations.ToArray();
        }
    }
}
