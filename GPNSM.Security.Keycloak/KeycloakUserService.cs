﻿using GPNSM.Common.Core.Exceptions;
using GPNSM.Security.Core.Classes;
using GPNSM.Security.Core.Contracts;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GPNSM.Security.Keycloak
{
    public class KeycloakUserService : IUserService
    {
        private readonly IOptions<OidcOptions> _options;

        public KeycloakUserService(IOptions<OidcOptions> options)
        {
            _options = options;
        }
        public async Task<Guid?> CreateUserAsync(CreateUserData userData)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            using (var client = new HttpClient(clientHandler))
            //using (var client = _clientFactory.CreateClient())
            {
                var formData = new FormUrlEncodedContent(new[]
               {
                    new KeyValuePair<string, string>(Consts.Keycloak.UserName, _options.Value.AdminName),
                    new KeyValuePair<string, string>(Consts.Keycloak.Password,  _options.Value.AdminPassword),
                    new KeyValuePair<string, string>(Consts.Keycloak.ClientId,  _options.Value.ClientId),
                    new KeyValuePair<string, string>(Consts.Keycloak.GrantType, Consts.Keycloak.GrantTypePassword),
                 });

                var adminResponse = await client.PostAsync(_options.Value.TokenUrl, formData);
                var adminJson = await adminResponse.Content.ReadAsStringAsync();
                var adminData = JsonConvert.DeserializeObject<OidcTokenResponse>(adminJson);

                var requestData = new
                {
                    username = userData.UserName,
                    enabled = true,
                    emailVerified = false,
                    email = userData.Email,
                    attributes = new { userType = userData.UserType },
                    credentials = new[] { 
                         new
                         {
                             type = "password",
                             value = userData.Password,
                             temporary = false
                         } 
                      }
                    };

                var content = new StringContent(JsonConvert.SerializeObject(requestData), Encoding.UTF8, "application/json");

                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {adminData.AccessToken}");


                var createUserResponse = await client.PostAsync(_options.Value.CreateUserUrl, content);

                if(createUserResponse.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    return Guid.Parse(createUserResponse.Headers.Location.ToString().Split('/').Last().TrimEnd('/'));
                }
                if (createUserResponse.StatusCode == System.Net.HttpStatusCode.Conflict)
                {
                    throw new BaseException(Keycloak.Resources.Keycloak.CreateUserConflict);
                }

                return Guid.Empty;
            }
        }
    }
}
