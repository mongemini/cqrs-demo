﻿using GPNSM.Common.Core.Extensions;
using GPNSM.Security.Core.Contracts;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace GPNSM.Security.Keycloak
{
    public class RefreshTokenRepositoryInMemory : IRefreshTokenRepository
    {
        private ConcurrentDictionary<string, string> _store = new ConcurrentDictionary<string, string>();

        public void AddRefreshToken(string asseccToken, string refreshToken)
        {
            //TODO: продумать и сделать систему очистки старых токенов, в самом токене есть время, когда он уже не действителен
            _store.AddOrUpdate(asseccToken, refreshToken);
        }

        public void DeleteRefreshToken(string asseccToken)
        {
            _store.Remove(asseccToken, out string value);
        }

        public string GetRefreshToken(string asseccToken)
        {
            return _store.GetValueOrDefault(asseccToken);
        }
    }
}
