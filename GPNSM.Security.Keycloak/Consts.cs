﻿namespace GPNSM.Security.Keycloak
{
    public static class  Consts
    {
        public static class Keycloak
        {
            public const string UserName = "username";
            public const string Password = "password";

            public const string Token = "token";
            public const string RefreshToken = "refresh_token";
            public const string ClientId = "client_id";

            public const string Scope = "scope";
            public const string ScopeContent = "openid profile email";

            public const string GrantType = "grant_type";
            public const string GrantTypePassword = "password";
            public const string GrantTypeRefreshToken = "refresh_token";

            public const string AuthenticationHeaderValue = "Bearer";
        }
    }
}
