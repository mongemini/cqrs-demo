﻿using GPNSM.Common.Core.Exceptions;
using GPNSM.Security.Core.Classes;
using GPNSM.Security.Core.Contracts;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;


namespace GPNSM.Security.Keycloak
{
    public class KeycloakAuthManager : IAuthManager
    {
        private readonly IOptions<OidcOptions> _options;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        //TODO: пожде переделать с использование фабрики, на данный момент не вермени на страивать https
        // private readonly IHttpClientFactory _clientFactory;

        public KeycloakAuthManager(IOptions<OidcOptions> options,
                                   IRefreshTokenRepository refreshTokenRepository)
        {
            _options = options;
            _refreshTokenRepository = refreshTokenRepository;
            //_clientFactory = clientFactory;
        }

        public async Task<OidcTokenResponse> LoginUser(string username, string password)
        {
            //TODO: разобраться как правильно подцепить сертификат и деплоить - это заглушка.
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            using (var client = new HttpClient(clientHandler))
            //using (var client = _clientFactory.CreateClient())
            {
                var formData = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>(Consts.Keycloak.UserName, username),
                    new KeyValuePair<string, string>(Consts.Keycloak.Password, password),
                    new KeyValuePair<string, string>(Consts.Keycloak.ClientId, _options.Value.ClientId),
                    new KeyValuePair<string, string>(Consts.Keycloak.GrantType, Consts.Keycloak.GrantTypePassword),
                    new KeyValuePair<string, string>(Consts.Keycloak.Scope, Consts.Keycloak.ScopeContent)
                 });

                var uri = _options.Value.TokenUrl;

                var res = await client.PostAsync(uri, formData);

                var json = await res.Content.ReadAsStringAsync();

                var tokenReponse = JsonConvert.DeserializeObject<OidcTokenResponse>(json);

                _refreshTokenRepository.AddRefreshToken(tokenReponse.AccessToken, tokenReponse.RefeshToken);
                tokenReponse.RefeshToken = string.Empty;

                return tokenReponse;
            }
        }

        public async Task<bool> LogoutUser(string accessToken)
        {
            var refreshToken = _refreshTokenRepository.GetRefreshToken(accessToken);

            if (string.IsNullOrEmpty(refreshToken))
                throw new BaseException();

            //TODO: разобраться как правильно подцепить сертификат и деплоить - это заглушка.
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            using (var client = new HttpClient(clientHandler))
            //using (var client = _clientFactory.CreateClient())
            {
                var formData = new FormUrlEncodedContent(new[]
                {
                  new KeyValuePair<string, string>(Consts.Keycloak.Token, accessToken), 
                  new KeyValuePair<string, string>(Consts.Keycloak.RefreshToken, refreshToken),
                  new KeyValuePair<string, string>(Consts.Keycloak.ClientId, _options.Value.ClientId),
                });

                var uri = _options.Value.LogoutUrl;

                var res = await client.PostAsync(uri, formData);

                return res.IsSuccessStatusCode;
            }
        }

        public async Task<OidcUser> GetUserInfo(string accessToken)
        {
            //TODO: разобраться как правильно подцепить сертификат и деплоить - это заглушка.
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            using (var client = new HttpClient(clientHandler))
            //using (var client = _clientFactory.CreateClient())
            {

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Consts.Keycloak.AuthenticationHeaderValue, accessToken);

                var formData = new FormUrlEncodedContent(new[]
                {
                  new KeyValuePair<string, string>(Consts.Keycloak.Token, accessToken)
               });

                var res = await client.GetAsync(_options.Value.UserInfoUrl);

                var json = await res.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<OidcUser>(json);
            }
        }

        public async Task<OidcTokenResponse> Refresh(string accessToken)
        {
            var refreshToken = _refreshTokenRepository.GetRefreshToken(accessToken);
            //TODO: check refreshToken for null because AddOrRefresh will call exception

            //TODO: разобраться как правильно подцепить сертификат и деплоить - это заглушка.
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            using (var client = new HttpClient(clientHandler))
            //using (var client = _clientFactory.CreateClient())
            {
                var formData = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>(Consts.Keycloak.RefreshToken, refreshToken),
                    new KeyValuePair<string, string>(Consts.Keycloak.GrantType, Consts.Keycloak.GrantTypeRefreshToken),
                    new KeyValuePair<string, string>(Consts.Keycloak.ClientId, _options.Value.ClientId),
                 });

                var res = await client.PostAsync(_options.Value.TokenUrl, formData);

                var json = await res.Content.ReadAsStringAsync();

                var tokenReponse = JsonConvert.DeserializeObject<OidcTokenResponse>(json);

                _refreshTokenRepository.DeleteRefreshToken(accessToken);
                _refreshTokenRepository.AddRefreshToken(tokenReponse.AccessToken, tokenReponse.RefeshToken);
                tokenReponse.RefeshToken = string.Empty;

                return tokenReponse;
            }
        }
    }
}
