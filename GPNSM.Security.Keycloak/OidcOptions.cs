﻿namespace GPNSM.Security.Keycloak
{
    public class OidcOptions
    {
        public string IssuerUrl { get; set; }
        public string ClientId { get; set; }
        public string TokenUrl { get; set; }
        public string LogoutUrl { get; set; }
        public string AuthUrl { get; set; }
        public string UserInfoUrl { get; set; }
        public string CreateUserUrl { get; set; }
        public string AdminName { get; set; }
        public string AdminPassword { get; set; }
    }
}
