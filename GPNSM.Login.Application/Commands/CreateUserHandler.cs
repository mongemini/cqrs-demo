﻿using AutoMapper;
using GPNSM.Domain.Core;
using GPNSM.Security.Core.Classes;
using GPNSM.Security.Core.Contracts;
using GPNSM.Shared.ExternalEvent;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Login.Application.Commands
{

    public class CreateUserHandler : IRequestHandler<CreateUserCommand, Guid?>
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IExternalDomainEventProvider _eventProvider;

        public CreateUserHandler(IMapper mapper, 
                                 IUserService userService,
                                 IExternalDomainEventProvider eventProvider) 
        {
            _userService = userService;
            _mapper = mapper;
            _eventProvider = eventProvider;
        }

        public async Task<Guid?> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var id = await _userService.CreateUserAsync(_mapper.Map<CreateUserData>(request.CreateUserData));
            if (id.HasValue)
            {
                var sharedData = _mapper.Map<CreatedUserSharedData>(request.CreateUserData);
                sharedData.Id = id.Value;
                await _eventProvider.PublishAsync(sharedData);
            }
            return id;

            //TODO: пока удобоно для теста очереди, потом убрать
            //var sharedData = _mapper.Map<CreatedUserSharedData>(request.CreateUserData);
            //sharedData.Id = Guid.NewGuid();
            //await _eventProvider.PublishAsync(sharedData);

            //return sharedData.Id;
        }
    }
}
