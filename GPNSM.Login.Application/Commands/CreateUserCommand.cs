﻿using GPNSM.Login.Application.ViewModels;
using MediatR;
using System;

namespace GPNSM.Login.Application.Commands
{
    public class CreateUserCommand : IRequest<Guid?>
    {
        public CreateUserCommand(CreateUserViewModel createUserData)
        {
            CreateUserData = createUserData;
        }

        public CreateUserViewModel CreateUserData { get; set; }
    }
}
