﻿using FluentValidation;
using GPNSM.Common.Core;
using GPNSM.Login.Application.Commands;

namespace GPNSM.Login.Application.Validators
{
    public class CreateUserValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserValidator()
        {
            RuleFor(c => c.CreateUserData.UserName).NotEmpty().MaximumLength(100);
            RuleFor(c => c.CreateUserData.JsonProperties).NotEmpty();
            RuleFor(c => c.CreateUserData.Password).NotEmpty().MaximumLength(50);
            RuleFor(c => c.CreateUserData.UserType).InclusiveBetween(1,2);
            RuleFor(c => c.CreateUserData.Email).NotEmpty().Matches(ConstsCore.RegularExpression.Email);
        }
    }
}
