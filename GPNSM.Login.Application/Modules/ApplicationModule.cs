﻿using Autofac;
using FluentValidation;
using GPNSM.Application.Core.Modules;
using MediatR;
using System;
using System.Linq;

namespace GPNSM.Login.Application.Modules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<CoreBehaviorModule>();
            builder.RegisterModule<NotificationModule>();
            builder.RegisterModule<RequestModule>();
            builder.RegisterModule<ValidatorModule>();
            base.Load(builder);
        }
    }

    public class NotificationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var notifications = ThisAssembly.GetTypes()
                .Where(t => t is INotification)
                .ToArray();
            Array.ForEach(notifications, e => builder.RegisterType(e).AsSelf().AsImplementedInterfaces());

            var notificationHandlers = ThisAssembly.GetTypes()
               .Where(t => t.IsClosedTypeOf(typeof(NotificationHandler<>)))
               .ToArray();
            Array.ForEach(notificationHandlers, e => builder.RegisterType(e).AsSelf().AsImplementedInterfaces());
            base.Load(builder);
        }
    }

    public class RequestModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var requests = ThisAssembly.GetTypes()
                .Where(t => t.IsClosedTypeOf(typeof(IRequest<>)))
                .ToArray();
            Array.ForEach(requests, e => builder.RegisterType(e).AsSelf().AsImplementedInterfaces());

            var handlers = ThisAssembly.GetTypes()
               .Where(t => t.IsClosedTypeOf(typeof(IRequestHandler<,>)))
               .ToArray();
            Array.ForEach(handlers, e => builder.RegisterType(e).AsSelf().AsImplementedInterfaces());
            base.Load(builder);
        }
    }

    public class ValidatorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var validators = ThisAssembly.GetTypes().Where(t => t.IsClosedTypeOf(typeof(IValidator<>))).ToArray();
            Array.ForEach(validators, v => builder.RegisterType(v).AsSelf().AsImplementedInterfaces());
            base.Load(builder);
        }
    }
}
