﻿
namespace GPNSM.Login.Application.ViewModels
{
    public class CreateUserViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int UserType { get; set; }
        public string JsonProperties { get; set; }
    }
}
