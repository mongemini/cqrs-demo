﻿using AutoMapper;
using GPNSM.Login.Application.ViewModels;
using GPNSM.Security.Core.Classes;
using GPNSM.Shared.ExternalEvent;

namespace GPNSM.Login.Application.Mapper
{
    public class ApplicationProfile : Profile
    {
        public ApplicationProfile()
        {
            CreateMap<CreateUserViewModel, CreateUserData>();
            CreateMap<CreateUserData, CreateUserViewModel>();

            CreateMap<CreateUserViewModel, CreatedUserSharedData>();
        }
    }
}
