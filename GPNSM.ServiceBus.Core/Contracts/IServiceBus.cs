﻿using System.Threading.Tasks;

namespace GPNSM.ServiceBus.Core.Contracts
{
    public interface IServiceBus
    {
        Task StartAsync();
        Task StopAsync();
        void Send<T>(T message) where T : class;
        void Publish<T>(T message) where T : class;
    }
}
