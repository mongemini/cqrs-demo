﻿namespace GPNSM.ServiceBus.Core.Contracts
{
    public interface IBusConfiguration
    {
        string Url { get; }
        string QueueName { get; }
        string UserName { get; }
        string Password { get; }
    }
}
