﻿using GPNSM.Blank.Domain.Services.Contracts;
using System.Threading.Tasks;

namespace GPNSM.Blank.Domain.Services
{
    public class SomeDomainService : ISomeDomainService
    {
        public Task<string> DoSomeInDomainAsync()
        {
            return Task.FromResult("SomeDomainService -> DoSomeInDomainAsync");
        }
    }
}
