﻿using System.Threading.Tasks;

namespace GPNSM.Blank.Domain.Services.Contracts
{
    public interface ISomeDomainService
    {
        Task<string> DoSomeInDomainAsync();
    }
}
