﻿using Autofac;
using GPNSM.Domain.Core;
using System;
using System.Linq;

namespace GPNSM.Blank.Domain.Modules
{
    public class DomainServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var requests = ThisAssembly.GetTypes()
             .Where(type => typeof(AggregateRoot).IsAssignableFrom(type))
             .Where(type => !type.IsAbstract)
             .ToArray();
            Array.ForEach(requests, e => builder.RegisterType(e).AsSelf().AsImplementedInterfaces());

            base.Load(builder);
        }
    }
}
