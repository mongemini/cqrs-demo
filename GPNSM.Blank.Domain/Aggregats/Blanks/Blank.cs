﻿using AutoMapper;
using GPNSM.Blank.Persistence.Entities;
using GPNSM.Blank.Persistence.Repositories.Contracts;
using GPNSM.Common.Core.Extensions;
using GPNSM.Domain.Core;
using GPNSM.Logger;
using GPNSM.Security.Core.Contracts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Domain.Aggregats.Blanks
{
    public class Blank: AggregateRoot
    {
        private readonly IBlankRepository _blankRepository;
        private readonly IBlankItemRepository _blankItemRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;
        private readonly ILog _logger;

        private Blank() : base(null) { }
        protected Blank(IDomainEventDispatcher dispatcher) : base(dispatcher) { }

        public string FirstName { get; set; }

        public string SecondName { get; set; }

        //public static Blank EmptyBlank()
        //{
        //    return new Blank();
        //}

        public Blank(IMapper mapper, 
                     IBlankRepository blankRepository,
                     IBlankItemRepository blankItemRepository,
                    // ICurrentUserService currentUserService,
                     ILog logger,
                     IDomainEventDispatcher dispatcher)
                    : base(dispatcher)
        {
            _mapper = mapper;
            _blankRepository = blankRepository;
            _blankItemRepository = blankItemRepository;
          //  _currentUserService = currentUserService;
            _logger = logger;
        }

        public async Task<Blank> GetBlankAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _blankRepository.Get(id).SingleOrDefaultAsync<BlankEntity, Blank>(_mapper, cancellationToken);
        }

        public async Task<Blank> CreateBlankAsync(Blank blank, CancellationToken cancellationToken)
        {
            _logger.Info("Blank -> CreateBlankAsync(only for example)");

            var dbEntity = _mapper.Map<BlankEntity>(blank);
          //  dbEntity.CreatedBy = _currentUserService.GetCurrentUserId();
            _blankRepository.Add(dbEntity);
            await _blankRepository.SaveAsync(cancellationToken);
            _dispatcher.Raise(this);

            return await GetBlankAsync(dbEntity.Id, cancellationToken);
        }

        public async Task<Blank> UpdateBlankAsync(Blank blank, CancellationToken cancellationToken)
        {
            var dbEntity = await _blankRepository.FindAsync(blank.Id, cancellationToken);
            blank.UpdatedBy = _currentUserService.GetCurrentUserId();
            if (dbEntity == null)
                throw new Exception($"Balnk not found, is {blank.Id}");

            _mapper.Map(blank, dbEntity);
            _blankRepository.Update(dbEntity);

            await _blankRepository.SaveAsync(cancellationToken);

            return await GetBlankAsync(blank.Id, cancellationToken);
        }

        public async Task<bool> DeleteBlankAsync(Guid id, CancellationToken cancellationToken)
        {
            _blankRepository.Delete(id);
            var dbEntity = await _blankRepository.FindAsync(id, cancellationToken);
            dbEntity.UpdatedBy = _currentUserService.GetCurrentUserId();
            var affectedRows = await _blankRepository.SaveAsync(cancellationToken);
            if (affectedRows == 0)
                throw new Exception($"Balnk not found, is {id}");

            return true;
        }

        public async Task<BlankItem> GetBlankItemAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _blankItemRepository.Get(id).SingleOrDefaultAsync<BlankItemEntity, BlankItem>(_mapper, cancellationToken);
        }

        public async Task<BlankItem> CreateBlankItemAsync(BlankItem blankItem, CancellationToken cancellationToken)
        {
            var dbEntity = _mapper.Map<BlankItemEntity>(blankItem);
            dbEntity.CreatedBy = _currentUserService.GetCurrentUserId();
            dbEntity.BlankId = Id;
            _blankItemRepository.Add(dbEntity);
            await _blankItemRepository.SaveAsync(cancellationToken);

            return await GetBlankItemAsync(dbEntity.Id, cancellationToken);
        }

        public async Task<bool> DeleteBlankItemAsync(Guid id, CancellationToken cancellationToken)
        {
            _blankItemRepository.Delete(id);
            var affectedRows = await _blankItemRepository.SaveAsync(cancellationToken);
            if (affectedRows == 0)
                throw new Exception($"Balnk Item not found, is {id}");

            return true;
        }


        public async Task<BlankItem> UpdateBlankItemAsync(BlankItem blankItem, CancellationToken cancellationToken)
        {
            var dbEntity = await _blankItemRepository.FindAsync(blankItem.Id, cancellationToken);
            if (dbEntity == null)
                throw new Exception($"Balnk Item not found, is {blankItem.Id}");
            _mapper.Map(blankItem, dbEntity);
            dbEntity.UpdatedBy = _currentUserService.GetCurrentUserId();

            _blankItemRepository.Update(dbEntity);

            await _blankItemRepository.SaveAsync(cancellationToken);

            return await GetBlankItemAsync(blankItem.Id, cancellationToken);
        }
        
        public class BlankItem : BaseEntityDomain<Guid>
        {
            protected BlankItem() { }
            internal BlankItem(string description)
            {
                Description = description;
            }

            public string Description { get; set; }
        }
    }
}
