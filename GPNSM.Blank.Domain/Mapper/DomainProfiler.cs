﻿using AutoMapper;
using GPNSM.Blank.Persistence.Entities;
using GPNSM.Data.Core.Entities;
using GPNSM.Domain.Core;
using System;

namespace GPNSM.Blank.Domain.Mapper
{
    public class DomainProfiler : Profile
    {
        public DomainProfiler()
        {
            CreateMap<BaseEntityDomain<Guid>, AuditableEntity<Guid>>()
           .ForMember(m => m.Id, e => e.Ignore())
           .ForMember(m => m.CreatedBy, e => e.Ignore())
           .ForMember(m => m.CreatedDate, e => e.Ignore())
            .IncludeAllDerived();

            CreateMap<Aggregats.Blanks.Blank, BlankEntity>();
            CreateMap<BlankEntity, Aggregats.Blanks.Blank>();

            CreateMap<Aggregats.Blanks.Blank.BlankItem, BlankItemEntity>();
            CreateMap<BlankItemEntity, Aggregats.Blanks.Blank.BlankItem>();
        }
    }

}
