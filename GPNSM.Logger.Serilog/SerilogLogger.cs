﻿using System;
using System.Threading.Tasks;
using Serilog;

namespace GPNSM.Logger.Serilog
{
    public class SerilogLogger : ILog
    {
        private readonly ILogger _serilogLogger;
        public SerilogLogger(ILogger serilogLogger)
        {
            _serilogLogger = serilogLogger;
        }
        public void Error(string message, Exception exception)
        {
            _serilogLogger.Error(exception, message);
        }

        public Task ErrorAsync(string message, Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Info(string message)
        {
            _serilogLogger.Information(message);
        }

        public Task InfoAsync(string message)
        {
            throw new NotImplementedException();
        }

        public void Trace(string message)
        {
            _serilogLogger.Verbose(message);
        }

        public Task TraceAsync(string message)
        {
            throw new NotImplementedException();
        }

        public void Warning(string message, Exception exception = null)
        {
            _serilogLogger.Warning(exception, message);
        }

        public Task WarningAsync(string message, Exception exception = null)
        {
            throw new NotImplementedException();
        }
    }
}
