﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;

namespace GPNSM.API.Common.Core.Filters
{
    public class CancellationTokenFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            context.ApiDescription.ParameterDescriptions
                .Where(pd =>
                    pd.ModelMetadata.ContainerType == typeof(System.Threading.CancellationToken) ||
                    pd.ModelMetadata.ContainerType == typeof(System.Threading.WaitHandle) ||
                    pd.ModelMetadata.ContainerType == typeof(Microsoft.Win32.SafeHandles.SafeWaitHandle))
                .ToList()
                .ForEach(
                    pd => {
                        if (operation.Parameters != null)
                        {
                            var cancellationTokenParameter = operation.Parameters.FirstOrDefault(p => p.Name.Equals(pd.Name, System.StringComparison.OrdinalIgnoreCase));
                            if (cancellationTokenParameter != null)
                                operation.Parameters.Remove(cancellationTokenParameter);
                        }
                    });
        }
    }
}
