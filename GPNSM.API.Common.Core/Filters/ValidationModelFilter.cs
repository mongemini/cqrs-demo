﻿using GPNSM.API.Common.Core.ErrorViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace GPNSM.API.Common.Core.Filters
{
    public class ValidationModelFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(new ValidationResultViewModel(context.ModelState));
            }
            base.OnActionExecuting(context);
        }
    }
}
