﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using GPNSM.API.Common.Core.ErrorViewModels;
using System.Net;
using GPNSM.Application.Core.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace GPNSM.API.Common.Core.Filters
{
    public class HandleErrorFilter : ExceptionFilterAttribute
    {
        private const string UnhandledException = "An unhandled exception has occurred while executing the request {0}";

        private readonly ILogger _logger;
        private readonly bool _isDevelopment;

        public HandleErrorFilter(ILoggerFactory loggerFactory, IWebHostEnvironment environment)
        {
            _isDevelopment = environment.IsDevelopment();
            _logger = loggerFactory.CreateLogger<HandleErrorFilter>();
        }

        public override void OnException(ExceptionContext context)
        {
            base.OnException(context);
            switch (context.Exception)
            {
                case NotFoundException ne:
                    SetNotFoundResult(context);
                    break; 
                case UnauthorizedAccessException ve:
                    SetUnauthorizedResult(context);
                    break;
                case AccessDeniedException ae:
                    SetForbiddenResult(context);
                    break;
                case ValidationException ve:
                    SetBadRequestResult(context, ve);
                    break;
                default:
                    SetExceptionResult(context);
                    break;
            }
            context.ExceptionHandled = true;
        }

        private void SetUnauthorizedResult(ExceptionContext context)
        {
            context.Result = new StatusCodeResult((int)HttpStatusCode.Unauthorized);
        }

        private void SetNotFoundResult(ExceptionContext context)
        {
            context.Result = new NotFoundObjectResult(new ErrorViewModel
            {
                Code = context.HttpContext.TraceIdentifier,
                Message = context.Exception.Message
            });
        }

        private void SetForbiddenResult(ExceptionContext context)
        {
            context.Result = new StatusCodeResult((int)HttpStatusCode.Forbidden);
        }

        private void SetBadRequestResult(ExceptionContext context, ValidationException exception)
        {
            context.Result = new BadRequestObjectResult(new ValidationResultViewModel(exception));
        }

        private void SetExceptionResult(ExceptionContext context)
        {
            _logger.LogError(context.Exception, String.Format(UnhandledException, context.HttpContext.TraceIdentifier));
            var result = new ObjectResult(new GenericErrorViewModel(context, _isDevelopment));
            result.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.Result = result;
        }
    }
}
