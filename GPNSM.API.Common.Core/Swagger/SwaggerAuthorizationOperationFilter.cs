﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;

namespace GPNSM.API.Common.Core.Swagger
{
    public class SwaggerAuthorizationOperationFilter : IOperationFilter
    {
        private readonly IConfiguration _configuration;
        public SwaggerAuthorizationOperationFilter(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var actionAttributes = context.MethodInfo.GetCustomAttributes(false);
            var controllerAttributes = context.MethodInfo.ReflectedType.GetCustomAttributes(false);

            // Check for authorize attribute
            var hasAuthorize = actionAttributes.OfType<AuthorizeAttribute>().Any() ||
                               controllerAttributes.OfType<AuthorizeAttribute>().Any();

            var allowAnonymous = actionAttributes.OfType<AllowAnonymousAttribute>().Any();

            if (hasAuthorize && !allowAnonymous)
            {
                if (operation.Parameters == null)
                    operation.Parameters = new List<OpenApiParameter>();

                operation.Responses.TryAdd("401", new OpenApiResponse { Description = "Unauthorized access, no access token provided by a client" });
                operation.Responses.TryAdd("403", new OpenApiResponse { Description = "Resource forbidden." });
                var securityRequirement = new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
                        },
                        new[] { _configuration.GetValue<string>(Consts.Swagger.SchemaName) }
                    }
                };

                operation.Security.Add(securityRequirement);
                operation.Security.Add(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}
                    }
                });
            }
        }
    }
}
