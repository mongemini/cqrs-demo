﻿using Autofac;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.API.Common.Core.Swagger
{
    public class SwaggerFluentValidatorFactory : ValidatorFactoryBase
    {
        private readonly IComponentContext context;

        public SwaggerFluentValidatorFactory(IComponentContext context)
        {
            this.context = context;
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            return context.ResolveOptional(validatorType) as IValidator;
        }
    }
}
