﻿using GPNSM.Common.Core.Extensions;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;

namespace GPNSM.API.Common.Core.Swagger
{
    public class SwaggerConfigureOptions : IConfigureOptions<SwaggerGenOptions>
    {
        readonly IApiVersionDescriptionProvider _provider;
        readonly IConfiguration _configuration;

        public SwaggerConfigureOptions(IApiVersionDescriptionProvider provider, IConfiguration configuration)
        {
            _provider = provider;
            _configuration = configuration;
        }

        public void Configure(SwaggerGenOptions options)
        {
            foreach (var description in _provider.ApiVersionDescriptions)
            {
                options.SwaggerDoc(description.GroupName, new OpenApiInfo
                {
                    Title = _configuration.GetValue<string>(Consts.Settings.Title),
                    Version = description.ApiVersion.ToString(),
                    Description = _configuration.GetValue<string>(Consts.Settings.Description)
                });
            }
            options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
            {
                Type = SecuritySchemeType.OAuth2,
                Flows = new OpenApiOAuthFlows
                {
                    Implicit = new OpenApiOAuthFlow
                    {
                        /// TODO: maybe relative path not this
                        AuthorizationUrl = new Uri($"{_configuration.GetValue<string>(Consts.Jwt.Authority)}/protocol/openid-connect/auth"),
                        Scopes = new Dictionary<string, string>()
                            {
                                { _configuration.GetValue<string>(Consts.Swagger.SchemaName), _configuration.GetValue<string>(Consts.Settings.Name) }
                            }
                    }
                }
            });
            options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                Name = "Authorization",
                Scheme = "Bearer",
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey
            });

            options.OperationFilter<SwaggerOperationFilter>();

            options.OperationFilter<SwaggerAuthorizationOperationFilter>();

            options.AddFluentValidationRules();
        }
    }
}
