﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.Extensions.Logging;
using System;

namespace GPNSM.API.Common.Core.ModelBinders
{
    public class ModelBinderProvider : IModelBinderProvider
    {
        public ModelBinderProvider(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }

        readonly ILoggerFactory _loggerFactory;

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(DateTime) ||
                context.Metadata.ModelType == typeof(DateTime?))
            {
                return new DateTimeModelBinder();
            }

            if (context.BindingInfo.BindingSource == null && context.Metadata.ModelType == typeof(string))
            {
                return new StringModelBinder(new SimpleTypeModelBinder(typeof(string), _loggerFactory));
            }

            return null;
        }
    }
}
