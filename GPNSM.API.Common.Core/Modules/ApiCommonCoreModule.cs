﻿using Autofac;
using GPNSM.API.Common.Core.Swagger;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.API.Common.Core.Modules
{
    public class ApiCommonCoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SwaggerFluentValidatorFactory>().AsImplementedInterfaces();

            base.Load(builder);
        }
    }
}
