﻿namespace GPNSM.API.Common.Core.ErrorViewModels
{
    public class ErrorViewModel
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
