﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace GPNSM.API.Common.Core.ErrorViewModels
{
    public class ValidationResultViewModel : ErrorViewModel
    {
        public List<ValidationErrorViewModel> Errors { get; }
        public ValidationResultViewModel() : this((ModelStateDictionary)null) { }

        public ValidationResultViewModel(ValidationException exception)
        {
            Message = exception.Message;
            if (exception.Errors != null)
                Errors = exception.Errors
                    .Select(a => new ValidationErrorViewModel(a.PropertyName, a.ErrorMessage)).ToList();
        }

        public ValidationResultViewModel(ModelStateDictionary modelState)
        {
            Message = @"Validation Failed";
            if (modelState != null)
            {
                Errors = new List<ValidationErrorViewModel>();
                modelState.Keys.ToList().ForEach(key => {
                    var messages = modelState[key].Errors.ToList().Select(x => {
                        var message = x.ErrorMessage;
                        if (string.IsNullOrWhiteSpace(message))
                            if (x.Exception is Newtonsoft.Json.JsonException)
                                message = $"The data in {key} field is of incorrect format";
                            else
                                message = x.Exception.Message;
                        return message;
                    });
                    if (messages.Count() == 0 && modelState[key].ValidationState == ModelValidationState.Unvalidated)
                        messages = new string[] { $"The data in {key} field is of incorrect format" };
                    messages.Distinct().ToList().ForEach(m => Errors.Add(new ValidationErrorViewModel(key, m)));
                });
            }
        }
    }
}
