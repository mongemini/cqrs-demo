﻿using Newtonsoft.Json;

namespace GPNSM.API.Common.Core.ErrorViewModels
{
    public class ValidationErrorViewModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Field { get; }

        public string Message { get; }

        public ValidationErrorViewModel(string field, string message)
        {
            Field = field != string.Empty ? field : null;
            Message = message;
        }
    }
}
