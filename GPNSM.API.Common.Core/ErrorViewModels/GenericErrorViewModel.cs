﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace GPNSM.API.Common.Core.ErrorViewModels
{
    public class GenericErrorViewModel : ErrorViewModel
    {
        public GenericErrorViewModel() { }
        public GenericErrorViewModel(ExceptionContext context, bool showStackTrace)
        {
            Code = context.HttpContext.TraceIdentifier;
            Init(context.Exception, showStackTrace);
        }
        public GenericErrorViewModel(Exception ex, bool showStackTrace)
        {
            Init(ex, showStackTrace);
        }

        private void Init(Exception error, bool showStackTrace)
        {
            Message = error.Message;
            if (showStackTrace)
                ParseStackTrace(error);
            if (error.InnerException != null)
                InnerException = new GenericErrorViewModel(error.InnerException, showStackTrace);
        }

        private void ParseStackTrace(Exception exception)
        {
            StackTrace = exception.StackTrace.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        }
        public GenericErrorViewModel InnerException { get; set; }
        public string[] StackTrace { get; set; }
    }
}
