﻿using GPNSM.Data.Core.Contracts;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GPNSM.API.Common.Core.Extensions
{
    public static class IHostExtensions
    {
        public static Task MigrateDatabasesAsync(this IHost host, CancellationToken cancellationToken)
        {
            var migrationManager = host.Services.GetRequiredService<IMigrationManager>();
            return migrationManager.MigrateAsync(cancellationToken);
        }
    }
}
