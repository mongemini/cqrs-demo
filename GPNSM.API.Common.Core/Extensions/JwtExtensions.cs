﻿using GPNSM.API.Common.Core.Resources;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace GPNSM.API.Common.Core.Extensions
{
    public static class JwtExtensions
    {
        public static void AddJwtAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            //for debbuging, show full error
            IdentityModelEventSource.ShowPII = true;
            var certificate = new X509Certificate2($"{AppDomain.CurrentDomain.BaseDirectory}{configuration[Consts.Jwt.CertFilePath]}");

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(configOptions =>
            {
                configOptions.SaveToken = true;

                configOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateAudience = false,

                    ValidateIssuer = true,
                    ValidIssuers = new[] { configuration[Consts.Jwt.Authority] },

                    IssuerSigningKey = new X509SecurityKey(certificate),
                    IssuerSigningKeyResolver =
                    (string token, SecurityToken securityToken, string kid, TokenValidationParameters validationParameters)
                    => new List<X509SecurityKey> {
                        new X509SecurityKey(certificate)
                    }
                };

                configOptions.Events = new JwtBearerEvents()
                {
                    //OnTokenValidated = authContext =>
                    //{
                    //    var clientId = ((JwtSecurityToken)authContext.SecurityToken).Subject;
                    //    return Task.CompletedTask;
                    //},
                    OnAuthenticationFailed = authContext =>
                    {
                        authContext.NoResult();
                        authContext.Response.StatusCode = 401;
                        authContext.Response.ContentType = "text/plain";

                        return authContext.Response.WriteAsync(
                            string.Format(API_Core.AuthenticationErrorMessage, authContext.Exception.ToString()));
                    }
                };
            });
            services.AddAuthorization();
        }
    }
}
