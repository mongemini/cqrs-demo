﻿using Autofac;
using MassTransit;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GPNSM.API.Common.Core.Extensions
{
    public static class MassTransitExtensions
    {
        public static void AddRabbitMq(this ContainerBuilder builder, IConfiguration configuration)
        {
            builder.AddMassTransit(x =>
            {
                x.AddBus(context => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    x.AddConsumersFromContainer(context);

                    cfg.Host(configuration[Consts.RabbitMq.Host],
                        h =>
                        {
                            h.Username(configuration[Consts.RabbitMq.UserName]);
                            h.Password(configuration[Consts.RabbitMq.Password]);
                            
                        });

                    cfg.ReceiveEndpoint(configuration[Consts.RabbitMq.QueueName], ec =>
                    {
                    });

                    cfg.ConfigureEndpoints(context);
                }));
            });

            builder.RegisterBuildCallback(context =>
            {
                context.Resolve<IBusControl>().Start();
            });
        }
    }
}
