﻿using Autofac;
using AutoMapper;
using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GPNSM.API.Common.Core.Extensions
{
    public static class ContainerBuilderExtensions
    {
        public static void RegisterAutoMapper(this ContainerBuilder builder, params Assembly[] assemblies)
        {
            builder.RegisterInstance(new Mapper(new MapperConfiguration(ctx =>
            {
                ctx.AddMaps(assemblies);
            }))).As<IMapper>();
        }

        public static void RegisterAutoMapper(this ContainerBuilder builder)
        {
            var assemblies = DependencyContext.Default.RuntimeLibraries
                .SelectMany(lib => lib.GetDefaultAssemblyNames(DependencyContext.Default)
                .Where(lib => lib.FullName.StartsWith(nameof(GPNSM)))
                .Select(Assembly.Load))
                .ToArray();

            builder.RegisterAutoMapper(assemblies);
        }

        public static void RegisterAutoMapperWithProfiles(this ContainerBuilder builder)
        {
            var assemblies = DependencyContext.Default.RuntimeLibraries
               .SelectMany(lib => lib.GetDefaultAssemblyNames(DependencyContext.Default)
               .Where(lib => lib.FullName.StartsWith(nameof(GPNSM)))
               .Select(Assembly.Load))
               .ToArray();

            var assembliesTypes = assemblies
                .SelectMany(lib => lib.GetTypes())
                .Where(p => typeof(Profile).IsAssignableFrom(p) && p.IsPublic && !p.IsAbstract)
                .Distinct()
                .ToArray();

            builder.RegisterTypes(assembliesTypes)
                .As<Profile>()
                .AutoActivate();

            builder.Register(ctx => new MapperConfiguration(cfg =>
            {
                var autoMapperProfiles = ctx.Resolve<IEnumerable<Profile>>();
                foreach (var profile in autoMapperProfiles)
                {
                    cfg.AddProfile(profile);
                }
            }));

            builder.Register(ctx => ctx.Resolve<MapperConfiguration>().CreateMapper())
                .As<IMapper>()
                .InstancePerLifetimeScope();
        }
    }
}
