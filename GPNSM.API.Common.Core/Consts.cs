﻿namespace GPNSM.API.Common.Core
{
    public static class Consts
    {
        public static class SupportedMimeTypes
        {
            public const string Json = "application/json";
            public const string FromData = "multipart/form-data";
        }

        public static class Swagger
        {
            public const string SchemaName = "Settings:SwaggerSchema";
            public const string KeyName = "Settings:SwaggerKey";
        }

        public static class Settings
        {
            public const string Name = "Settings:Name";
            public const string Title = "Settings:Title";
            public const string Description = "Settings:Description";
        }

        public static class Jwt
        {
            public const string Authority = "Jwt:Authority";
            public const string CertFilePath = "Jwt:CertFilePath";
        }

        public static class RabbitMq
        {
            public const string Host = "RabbitMq:Host";
            public const string QueueName = "RabbitMq:QueueName";
            public const string UserName = "RabbitMq:UserName";
            public const string Password = "RabbitMq:Password";
        }
    }
}
