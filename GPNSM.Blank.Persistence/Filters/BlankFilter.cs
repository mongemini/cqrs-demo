﻿using GPNSM.Blank.Persistence.Criterias;
using GPNSM.Blank.Persistence.Entities;
using GPNSM.Data.Core.Criterias;
using GPNSM.Data.Core.Criterias.Helpers;
using System;
using System.Linq.Expressions;

namespace GPNSM.Blank.Persistence.Filters
{
    public class BlankFilter : PagedCriteria<BlankEntity>
    {
        public Guid? CreatedBy { get; set; }

        public override Expression<Func<BlankEntity, bool>> Build()
        {
            var criteria = True;
            if (CreatedBy.HasValue)
                criteria = criteria.And(new BlankCreatedBy(CreatedBy.Value));
  
            if (String.IsNullOrEmpty(Sort))
                SetSortBy(a => a.CreatedDate);
            return criteria.Build();
        }
    }
}
