﻿using Autofac;
using GPNSM.Repositories.Core.Contracts;
using System.Linq;
using System.Reflection;

namespace GPNSM.Blank.Persistence.Modules
{
    public class RepositoryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var repoTypes = Assembly.GetExecutingAssembly()
                .GetTypes().
                Where(t => t.IsClosedTypeOf(typeof(IRepository<,>)) && !t.IsAbstract).ToList();

            foreach (var type in repoTypes)
                builder.RegisterType(type).AsImplementedInterfaces();
            base.Load(builder);
        }
    }
}
