﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GPNSM.Blank.Persistence.Migrations
{
    public partial class extended_blank : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Blanks",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecondName",
                table: "Blanks",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Blanks");

            migrationBuilder.DropColumn(
                name: "SecondName",
                table: "Blanks");
        }
    }
}
