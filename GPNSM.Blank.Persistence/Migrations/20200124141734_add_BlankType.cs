﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace GPNSM.Blank.Persistence.Migrations
{
    public partial class add_BlankType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BlankTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlankTypes", x => x.Id);
                });

            migrationBuilder.Sql(@"INSERT INTO public.""BlankTypes"" (""Id"", ""Value"")
                                   VALUES
                                            (1, 'test1'),
                                            (2, 'test2'),
                                            (3, 'test3')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlankTypes");
        }
    }
}
