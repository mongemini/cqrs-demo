﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GPNSM.Blank.Persistence.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Blanks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blanks", x => x.Id);
                });
            migrationBuilder.Sql(@"CREATE EXTENSION IF NOT EXISTS ""uuid-ossp"";
                                   INSERT INTO public.""Blanks""
                                            (""Id"", ""CreatedDate"", ""CreatedBy"", ""UpdatedDate"", ""UpdatedBy"", ""IsDeleted"")
                                   VALUES
                                            (uuid_generate_v1(), now() at time zone 'utc', uuid_generate_v1(), now() at time zone 'utc', uuid_generate_v1(), false)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Blanks");
        }
    }
}
