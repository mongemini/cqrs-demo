﻿using GPNSM.Blank.Persistence.Entities;
using GPNSM.Data.Core.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GPNSM.Blank.Persistence.Configurations
{
    public class BlankItemEntityConfiguration : EntityTypeConfiguration<BlankItemEntity>
    {
        public override void Configure(EntityTypeBuilder<BlankItemEntity> builder)
        {
            builder.HasOne(m => m.Blank).WithMany(m => m.Items).HasForeignKey(m => m.BlankId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        }
    }
}