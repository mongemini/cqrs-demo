﻿using GPNSM.Blank.Persistence.Entities;
using GPNSM.Blank.Persistence.Entities.Dictionaries;
using GPNSM.Data.Core;
using GPNSM.Data.Core.Configuration;
using Microsoft.EntityFrameworkCore;

namespace GPNSM.Blank.Persistence.Context
{
    public class BlankContext: BaseDbContext, IBlankContext
    {
        public BlankContext(DbContextOptions<BlankContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            DbConfigurator.Configure<BlankContext>(modelBuilder);
        }

        public DbSet<BlankEntity> Blanks { get; set; }

        public DbSet<BlankItemEntity> BlankItems { get; set; }

        public DbSet<BlankType> BlankTypes { get; set; }

    }
}
