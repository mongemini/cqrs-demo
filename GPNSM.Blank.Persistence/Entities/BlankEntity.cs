﻿using GPNSM.Data.Core.Entities;
using System.Collections.Generic;

namespace GPNSM.Blank.Persistence.Entities
{
    public class BlankEntity: ArchivableEntity
    {
        public BlankEntity()
        {
            Items = new List<BlankItemEntity>();
        }
        public string FirstName { get; set; }

        public string SecondName { get; set; }


        public virtual ICollection<BlankItemEntity> Items { get; set; }
    }
}
