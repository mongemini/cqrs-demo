﻿using GPNSM.Data.Core.Entities;
using System;

namespace GPNSM.Blank.Persistence.Entities
{
    public class BlankItemEntity: ArchivableEntity
    {
        public string Description { get; set; }


        public Guid BlankId { get; set; }

        public BlankEntity Blank { get; set; }
    }
}
