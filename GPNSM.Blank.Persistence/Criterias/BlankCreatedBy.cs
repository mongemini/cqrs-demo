﻿using GPNSM.Blank.Persistence.Entities;
using GPNSM.Data.Core.Criterias;
using System;
using System.Linq.Expressions;

namespace GPNSM.Blank.Persistence.Criterias
{
    public class BlankCreatedBy : CriteriaBase<BlankEntity>
    {
        private readonly Guid _createdBy;
        public BlankCreatedBy(Guid createdBy)
        {
            _createdBy = createdBy;
        }

        public override Expression<Func<BlankEntity, bool>> Build()
        {
            return a => a.CreatedBy == _createdBy;
        }
    }
}
