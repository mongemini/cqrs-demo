﻿using GPNSM.Blank.Persistence.Entities;
using GPNSM.Data.Core.Criterias;
using System;
using System.Linq.Expressions;

namespace GPNSM.Blank.Persistence.Criterias
{
    public class BlankItemsByBlankId : CriteriaBase<BlankItemEntity>
    {
        private readonly Guid _blankId;
        public BlankItemsByBlankId(Guid blankId)
        {
            _blankId = blankId;
        }

        public override Expression<Func<BlankItemEntity, bool>> Build()
        {
            return a => a.BlankId == _blankId;
        }
    }
}
