﻿using GPNSM.Blank.Persistence.Context;
using GPNSM.Blank.Persistence.Entities;
using GPNSM.Blank.Persistence.Repositories.Contracts;
using GPNSM.Repositories.Core;

namespace GPNSM.Blank.Persistence.Repositories
{
    public class BlankRepository : ArchivableRepository<BlankEntity>, IBlankRepository
    {
        public BlankRepository(IBlankContext context) : base(context) { }
    }
}
