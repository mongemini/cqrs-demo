﻿using GPNSM.Blank.Persistence.Entities.Dictionaries;
using GPNSM.Repositories.Core.Contracts;

namespace GPNSM.Blank.Persistence.Repositories.Dictionaries
{
    public interface IBlankTypeRepository : IDictionaryRepository<BlankType> {}
}
