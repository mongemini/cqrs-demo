﻿using GPNSM.Blank.Persistence.Context;
using GPNSM.Blank.Persistence.Entities.Dictionaries;
using GPNSM.Repositories.Core;

namespace GPNSM.Blank.Persistence.Repositories.Dictionaries
{
    public class BlankTypeRepository : DictionaryRepository<BlankType>, IBlankTypeRepository
    {
        public BlankTypeRepository(IBlankContext context) : base(context)
        {
        }
    }
}
