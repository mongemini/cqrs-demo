﻿using GPNSM.Blank.Persistence.Entities;
using GPNSM.Repositories.Core.Contracts;

namespace GPNSM.Blank.Persistence.Repositories.Contracts
{
    public interface IBlankItemRepository : IArchivableRepository<BlankItemEntity>
    {
    }
}
