﻿using GPNSM.Data.Core.Contracts;

namespace GPNSM.Blank.Persistence.Context
{
    public interface IBlankContext: IDbContext
    {
    }
}
