﻿using GPNSM.Data.Core;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace GPNSM.Blank.Persistence.Context
{
    public class BlankContextModule : DbContextModule<BlankContext>, IDesignTimeDbContextFactory<BlankContext> 
    {
        public BlankContextModule() : this(GetConfiguration()) { }
        public BlankContextModule(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
