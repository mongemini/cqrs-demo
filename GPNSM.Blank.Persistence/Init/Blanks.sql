﻿
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO public."Blanks" ("Id", "CreatedDate", "CreatedBy", "UpdatedDate", "UpdatedBy", "IsDeleted")
VALUES
        (uuid_generate_v1(), now() at time zone 'utc', uuid_generate_v1(), now() at time zone 'utc', uuid_generate_v1(), false)