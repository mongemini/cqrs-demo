﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Caching.Utilities
{
    public static class DistributedCacheExtensions
    {
        static DistributedCacheExtensions()
        {
            _settings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                NullValueHandling = NullValueHandling.Ignore
            };
            _options = new DistributedCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
            };
        }
        static readonly DistributedCacheEntryOptions _options;
        static readonly JsonSerializerSettings _settings;

        public static TResult GetOrCreate<TResult>(this IDistributedCache cache, string key, DistributedCacheEntryOptions options, Func<TResult> func)
        {
            TResult obj = default(TResult);
            string value;
            try
            {
                value = cache.GetString(key);
                if (value != null)
                {
                    obj = JsonConvert.DeserializeObject<TResult>(value, _settings);
                    return obj;
                }
            }
            catch { }
            obj = func();
            try
            {
                value = JsonConvert.SerializeObject(obj, _settings);
                cache.SetString(key, value, options);
            }
            catch { }
            return obj;
        }

        public static TResult GetOrCreate<TResult>(this IDistributedCache cache, string key, Func<TResult> func)
        {
            return cache.GetOrCreate(key, _options, func);
        }

        public static async Task<TResult> GetOrCreateAsync<TResult>(this IDistributedCache cache, string key, DistributedCacheEntryOptions options, Func<Task<TResult>> func, CancellationToken cancellationToken)
        {
            TResult obj = default(TResult);
            string value;
            try
            {
                value = await cache.GetStringAsync(key, cancellationToken);
                if (value != null)
                {
                    obj = JsonConvert.DeserializeObject<TResult>(value, _settings);
                    return obj;
                }
            }
            catch { }
            obj = await func();
            try
            {
                value = JsonConvert.SerializeObject(obj, _settings);
                await cache.SetStringAsync(key, value, options, cancellationToken);
            }
            catch { }
            return obj;
        }

        public static Task<TResult> GetOrCreateAsync<TResult>(this IDistributedCache cache, string key, Func<Task<TResult>> func, CancellationToken cancellationToken)
        {
            return cache.GetOrCreateAsync(key, _options, func, cancellationToken);
        }
    }
}
