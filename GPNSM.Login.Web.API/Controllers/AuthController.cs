﻿using GPNSM.Login.Web.API.ViewModels;
using GPNSM.Security.Core.Contracts;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GPNSM.Login.Web.API.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersionNeutral]
    [Authorize]
    public class AuthController : Controller
    {
        private readonly IAuthManager _authManager;

        public AuthController( IAuthManager authManager, ICurrentUserService test)
        {
            _authManager = authManager;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel loginData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var token = await _authManager.LoginUser(loginData.UserName, loginData.Password);

            return Ok(token);
        }

        [HttpPost("logout")]
        public async Task<IActionResult> LogOut()
        {
            var token = await _authManager.LogoutUser(await GetAccessTokenAsync());

            return Ok(token);
        }

        [HttpGet("userInfo")]
        public async Task<IActionResult> GetUserInfo()
        {
            var info = await _authManager.GetUserInfo(await GetAccessTokenAsync());

            return Ok(info);
        }

        [HttpPost("refresh")]
        [AllowAnonymous]
        public async Task<IActionResult> Refresh(string accessToken)
        {
            if(string.IsNullOrEmpty(accessToken))
            {
                return BadRequest();
            }

            var newData = await _authManager.Refresh(accessToken);

            return Ok(newData);
        }

        private async Task<string> GetAccessTokenAsync()
        {
            return await HttpContext.GetTokenAsync("access_token");
        }
    }
}