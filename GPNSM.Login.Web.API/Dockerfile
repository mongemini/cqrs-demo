#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["GPNSM.Login.Web.API/GPNSM.Login.Web.API.csproj", "GPNSM.Login.Web.API/"]
COPY ["GPNSM.API.Common.Core/GPNSM.API.Common.Core.csproj", "GPNSM.API.Common.Core/"]
COPY ["GPNSM.Common.Core/GPNSM.Common.Core.csproj", "GPNSM.Common.Core/"]
COPY ["GPNSM.Data.Core/GPNSM.Data.Core.csproj", "GPNSM.Data.Core/"]
COPY ["GPNSM.Contracts.Data.Entities/GPNSM.Contracts.Data.Entities.csproj", "GPNSM.Contracts.Data.Entities/"]
COPY ["GPNSM.Application.Core/GPNSM.Application.Core.csproj", "GPNSM.Application.Core/"]
COPY ["GPNSM.Domain.Core/GPNSM.Domain.Core.csproj", "GPNSM.Domain.Core/"]
COPY ["GPNSM.Repositories.Core/GPNSM.Repositories.Core.csproj", "GPNSM.Repositories.Core/"]
COPY ["GPNSM.Logger/GPNSM.Logger.csproj", "GPNSM.Logger/"]
COPY ["GPNSM.Caching/GPNSM.Caching.csproj", "GPNSM.Caching/"]
COPY ["GPNSM.Security.Keycloak/GPNSM.Security.Keycloak.csproj", "GPNSM.Security.Keycloak/"]
COPY ["GPNSM.Security.Core/GPNSM.Security.Core.csproj", "GPNSM.Security.Core/"]
RUN dotnet restore "GPNSM.Login.Web.API/GPNSM.Login.Web.API.csproj"
COPY . .
WORKDIR "/src/GPNSM.Login.Web.API"
RUN dotnet build "GPNSM.Login.Web.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "GPNSM.Login.Web.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "GPNSM.Login.Web.API.dll"]