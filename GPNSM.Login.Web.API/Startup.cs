using Autofac;
using GPNSM.API.Common.Core.Extensions;
using GPNSM.API.Common.Core.Modules;
using GPNSM.API.Common.Core.Swagger;
using GPNSM.Application.Core.Modules;
using GPNSM.Login.Application.Modules;
using GPNSM.Security.Core.Contracts;
using GPNSM.Security.Core.Modules;
using GPNSM.Security.Keycloak;
using MediatR.Extensions.Autofac.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using GPNSM.API.Common.Core.Filters;
using GPNSM.ServiceBus.RabbitMq;

namespace GPNSM.Login.Web.API
{
    public class Startup
    {
        private const string _corsName = "GPNSM.Login.API";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCompressionServices();

            //TODO: пожде переделать с использование фабрики, на данный момент не вермени на страивать https
            //services.AddHttpClient("yowkoblog", c =>
            //{
            //    c.BaseAddress = new Uri("https://blog.yowko.com/");
            //})
            //.ConfigurePrimaryHttpMessageHandler(h =>
            //{
            //    var clientHandler = new HttpClientHandler();
            //    clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            //    return clientHandler;
            //});

            services.AddMvc();
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddCors(o => o.AddPolicy(_corsName, options =>
            {
                options.AllowAnyOrigin()
                       .AllowAnyHeader()
                       .AllowAnyMethod();
            }));
            services.AddMvcCore(options =>
            {
                options.EnableEndpointRouting = false;
				options.Filters.Add(typeof(HandleErrorFilter));
            })
                .AddApiExplorer()
                .SetCompatibilityVersion(CompatibilityVersion.Latest)
                .AddControllersAsServices();

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
            }).AddVersionedApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });

            services.AddJwtAuthentication(Configuration);

            services.Configure<OidcOptions>(Configuration.GetSection("oidc"));

            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, SwaggerConfigureOptions>();
            services.AddSwaggerGen();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IRefreshTokenRepository, RefreshTokenRepositoryInMemory>();
            services.AddTransient<IAuthManager, KeycloakAuthManager>();
           // services.AddTransient<IUserService, KeycloakUserService>();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterAutoMapper();

            builder.RegisterModule<CoreBehaviorModule>();
            builder.RegisterModule<ApiCommonCoreModule>();

            builder.RegisterModule<ApplicationModule>();

            builder.RegisterModule<SecurityModule>();
            builder.RegisterType<KeycloakUserService>().AsSelf().AsImplementedInterfaces();

            builder.AddMediatR(typeof(Program).Assembly);

            builder.AddRabbitMq(Configuration);
            builder.RegisterType<RabbitMqDomainEventProvider>().AsSelf().AsImplementedInterfaces();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                app.UsePathBase(pathBase);
            }

            app.UseCors(_corsName);

            app.UseStaticFiles();

            app.UseResponseCompression();

            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    // Need create client of keycloak for service and set redirect valid and enable Implicit flow
                    //options.OAuth2RedirectUrl($"{Configuration.GetValue<string>("BaseUrl") ?? "http://localhost"}/swagger/oauth2-redirect.html");
                    //options.OAuthClientId(Configuration.GetValue<string>(Consts.Swagger.KeyName));
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint(
                            $"{pathBase}/swagger/{description.GroupName}/swagger.json",
                            description.GroupName.ToUpperInvariant());
                    }
                });
        }
    }
}
