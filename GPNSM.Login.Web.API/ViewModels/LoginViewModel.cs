﻿using System.ComponentModel.DataAnnotations;

namespace GPNSM.Login.Web.API.ViewModels
{
    public class LoginViewModel
    {
        [Required(AllowEmptyStrings = false)]
        public string UserName { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }
    }
}
