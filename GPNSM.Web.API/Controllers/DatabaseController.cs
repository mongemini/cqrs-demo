﻿using GPNSM.Data.Core.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Web.API.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersionNeutral]
    public class DatabaseController : ControllerBase
    {
        private readonly IMigrationManager _migrationManager;
        public DatabaseController(IMigrationManager migrationManager)
        {
            _migrationManager = migrationManager;
        }

        /// <summary>
        /// Get Migrations List
        /// </summary>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>Returns the dictionary with all pending migrations split by the context</returns>
        /// <response code="200">Dictionary with all pending migrations split by the context</response>
        /// <response code="403">Access Denied</response>
        /// <response code="500">Unexpected server error</response>
        [HttpGet]
        public async Task<IActionResult> IndexAsync(CancellationToken cancellationToken)
        {
            var result = await _migrationManager.GetPendingMigrationsAsync(cancellationToken);
            return Ok(result);
        }

        /// <summary>
        /// Apply Migrations
        /// </summary>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>Returns the dictionary with all pending migrations split by the context</returns>
        /// <response code="200">Dictionary with all pending migrations split by the context</response>
        /// <response code="403">Access Denied</response>
        /// <response code="500">Unexpected server error</response>
        [HttpPost]
        public async Task<IActionResult> PostAsync(CancellationToken cancellationToken)
        {
            await _migrationManager.MigrateAsync(cancellationToken);
            return await IndexAsync(cancellationToken);
        }
    }
}