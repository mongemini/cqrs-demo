﻿using GPNSM.API.Common.Core.ErrorViewModels;
using GPNSM.Application.Core.ViewModels;
using GPNSM.Blank.Application.Commands.Blanks;
using GPNSM.Blank.Application.Commands.Blanks.BlankItems;
using GPNSM.Blank.Application.Queries.Blanks;
using GPNSM.Blank.Application.Queries.Blanks.BlankItems;
using GPNSM.Blank.Application.ViewModels.Blank;
using GPNSM.Blank.Application.ViewModels.Blank.BlankItem;
using GPNSM.Web.API.Resources;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Web.API.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    //[Authorize]
    [ApiVersionNeutral]
    public class BlanksController : ControllerBase
    {
        private const string GetBlankByIdName = "GetByIdAsync";

        private readonly IMediator _mediator;
        private readonly IStringLocalizer<BlanksController> _localizer;

        public BlanksController(IMediator mediator,
                               IStringLocalizer<BlanksController> localizer)
        {
            _mediator = mediator;
            _localizer = localizer;
        }

        /// <summary>
        /// Test resource file
        /// </summary>
        [HttpGet("resource")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult GetResource()
        {
            return Ok(BlankAPI.BlankAPISomeText);
        }

        /// <summary>
        /// Get Blanks
        /// </summary>
        /// <remarks>This method returns the list of blanks</remarks>
        /// <param name="request">request parameters</param>
        /// <param name="cancellationToken">cancellation token</param>
        /// <returns>The list of blanks</returns>
        /// <response code="200">The list of blanks</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="500">Unhandled server error</response>
        [HttpGet]
        [ProducesResponseType(typeof(IList<object>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllBlankAsync([FromQuery] GetBlanksRequest request, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(request));
        }

        /// <summary>
        /// Get Blank
        /// </summary>
        /// <remarks>Method returns the blank by unique Id</remarks>
        /// <param name="id">The unique identifier</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>Blank</returns>
        /// <response code="200">Blank</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="404">Requested blank is not found</response>
        /// <response code="500">Unhandled server error</response>
        [HttpGet("{id:guid}", Name = GetBlankByIdName)]
        [ProducesResponseType(typeof(BlankViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetBlankByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new GetBlankRequest(id)));
        }

        /// <summary>
        /// Create Blank
        /// </summary>
        /// <remarks>Method creates blank by passed payload to the body</remarks>
        /// <param name="request">Payload</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Created blank</returns>
        /// <response code="201">blank</response>
        /// <response code="400">Payload is invalid</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="500">Unhandled server error</response>
        [HttpPost]
        //[ProducesResponseType(typeof(BlankViewModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BlankViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateBlankAsync([FromBody] CreateBlankCommand request, CancellationToken cancellationToken)
        {
            // TODO: надо подумать что лучше возвращать объект или линку на 
            return Ok(await _mediator.Send(request, cancellationToken));
            // return CreatedAtRoute(GetBlankByIdName, new { id = response.Id }, response);
        }

        /// <summary>
        /// Update Blank
        /// </summary>
        /// <remarks>Method updates blank by passed payload to the body</remarks>
        /// <param name="request">Payload</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Updated blank</returns>
        /// <response code="200">blank</response>
        /// <response code="400">Payload is invalid</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="404">Requested blank is not found</response>
        /// <response code="500">Unhandled server error</response>
        [HttpPut]
        [ProducesResponseType(typeof(BlankViewModel), StatusCodes.Status200OK)]
        //TODO: сделать модель для ошибки
        [ProducesResponseType(typeof(object), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateBlankAsync([FromBody] UpdateBlankCommand request, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(request, cancellationToken));
        }

        /// <summary>
        /// Patch Blank
        /// </summary>
        /// <remarks>Method updates blank by passed payload to the body</remarks>
        /// <param name="id">Unqiue Id</param>
        /// <param name="request">Payload</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Updated blank</returns>
        /// <response code="200">Blank</response>
        /// <response code="400">Payload is invalid</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="404">Requested blank is not found</response>
        /// <response code="500">Unhandled server error</response>
        //[HttpPatch("{id:guid}")]
        //[ProducesResponseType(typeof(object), StatusCodes.Status200OK)]
        //[ProducesResponseType(typeof(object), StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(typeof(ErrorModel), StatusCodes.Status404NotFound)]
        //public async Task<IActionResult> PatchAsync(Guid id, [FromBody] JsonPatchDocument<object> request, CancellationToken cancellationToken)
        //{
        //    return Ok("patch");
        //}

        /// <summary>
        /// Delete Blank
        /// </summary>
        /// <remarks>Method deletes blank by its unique Id</remarks>
        /// <param name="id">Customer Unique Id</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Empty Response</returns>
        /// <response code="200">Empty Response</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="404">Requested blank is not found</response>
        /// <response code="500">Unhandled server error</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteBlankAsync(Guid id, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new DeleteBlankCommand(id), cancellationToken));
        }

        /// <summary>
        /// Get Blank Types
        /// </summary>
        /// <remarks>This method returns the list of blank types</remarks>
        /// <param name="cancellationToken">cancellation token</param>
        /// <returns>The list of blank types</returns>
        /// <response code="200">The list of blank types</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="500">Unhandled server error</response>
        [HttpGet("types")]
        [ProducesResponseType(typeof(DictionaryView), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetBlankTypesAsync(CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new GetBlankTypesRequest()));
        }

        /// <summary>
        /// Get Blank Items
        /// </summary>
        /// <remarks>Method returns the blank items by blank Id</remarks>
        /// <param name="id">The unique blank identifier</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>List of blank items</returns>
        /// <response code="200">Balnk items</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="500">Unhandled server error</response>
        [HttpGet("{id:guid}/items")]
        [ProducesResponseType(typeof(BlankItemDto[]), StatusCodes.Status200OK)]
        public async Task<IActionResult> GeBlankItemsAsync(Guid id, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new GetBlankItemsRequest(id), cancellationToken));
        }

        /// <summary>
        /// Get Blank Item
        /// </summary>
        /// <remarks>Method returns the blank item by Id</remarks>
        /// <param name="id">The unique blank item identifier</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>Blank item</returns>
        /// <response code="200">Balnk item</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="500">Unhandled server error</response>
        [HttpGet("items/{id:guid}")]
        [ProducesResponseType(typeof(BlankItemDto), StatusCodes.Status200OK)]
        public async Task<IActionResult> GeBlankItemAsync(Guid id, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new GetBlankItemRequest(id), cancellationToken));
        }

        /// <summary>
        /// Create Blank Item
        /// </summary>
        /// <remarks>Method creates blank item by passed payload to the body</remarks>
        /// <param name="request">Payload</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Created blank</returns>
        /// <response code="201">blank item</response>
        /// <response code="400">Payload is invalid</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="500">Unhandled server error</response>
        [HttpPost("items")]
        //[ProducesResponseType(typeof(BlankViewModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BlankItemViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateBlankItemAsync([FromBody] CreateBlankItemCommand request, CancellationToken cancellationToken)
        {
            var response = await _mediator.Send(request, cancellationToken);
            return Ok(response);
        }

        /// <summary>
        /// Delete Blank Itm
        /// </summary>
        /// <remarks>Method deletes blank item by its unique Id</remarks>
        /// <param name="id">Customer Unique Id</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Empty Response</returns>
        /// <response code="200">Empty Response</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="404">Requested blank item is not found</response>
        /// <response code="500">Unhandled server error</response>
        [HttpDelete("items/{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteBlankItemAsync(Guid id, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new DeleteBlankItemCommand(id), cancellationToken));
        }

        /// <summary>
        /// Update Blank Item
        /// </summary>
        /// <remarks>Method updates blank item by passed payload to the body</remarks>
        /// <param name="request">Payload</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Updated blank item</returns>
        /// <response code="200">blank item</response>
        /// <response code="400">Payload is invalid</response>
        /// <response code="401">Unauthorized access, no access token provided by a client</response>
        /// <response code="403">Resource forbidden.</response>
        /// <response code="404">Requested blank is not found</response>
        /// <response code="500">Unhandled server error</response>
        [HttpPut("items")]
        [ProducesResponseType(typeof(BlankItemViewModel), StatusCodes.Status200OK)]
        //TODO: сделать модель для ошибки
        [ProducesResponseType(typeof(object), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateBlankItemAsync([FromBody] UpdateBlankItemCommand request, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(request, cancellationToken));
        }
    }
}