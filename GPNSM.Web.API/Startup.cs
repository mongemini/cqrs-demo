using GPNSM.Blank.Persistence.Context;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Autofac;
using GPNSM.Blank.Application.Modules;
using MediatR.Extensions.Autofac.DependencyInjection;
using GPNSM.Data.Core.Contracts;
using GPNSM.Data.Core.Migrations;
using GPNSM.Security.Core.Modules;
using GPNSM.API.Common.Core.Extensions;
using GPNSM.Blank.Persistence.Modules;
using GPNSM.Blank.Domain.Modules;
using AutofacSerilogIntegration;
using GPNSM.Logger.Serilog;
using GPNSM.Logger;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using GPNSM.API.Common.Core.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using GPNSM.API.Common.Core;
using GPNSM.Caching.Modules;
using GPNSM.ServiceBus.RabbitMq;
using GPNSM.API.Common.Core.Filters;

namespace GPNSM.Web.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCompressionServices();

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddCors(o => o.AddPolicy("GPNSM.Blank.API", options =>
            {
                options.WithOrigins()
                       .AllowAnyHeader()
                       .AllowAnyMethod()
                       .AllowCredentials();
            }));

            services.AddMvcCore(options =>
            {
                options.EnableEndpointRouting = false;
                options.Filters.Add(typeof(HandleErrorFilter));
            })
                .AddApiExplorer()
                .SetCompatibilityVersion(CompatibilityVersion.Latest)
                .AddControllersAsServices();

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
            }).AddVersionedApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });

            services.AddJwtAuthentication(Configuration);

            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, SwaggerConfigureOptions>();
            services.AddSwaggerGen();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterAutoMapper();

            //for direct injection Serilog
            builder.RegisterLogger();
            //injection with help abstraction
            builder.RegisterType<SerilogLogger>().As<ILog>();

            builder.RegisterModule(new BlankContextModule(Configuration));
            builder.RegisterType<MigrationManager>().As<IMigrationManager>();

            builder.RegisterModule<RepositoryModule>();
            builder.RegisterModule<MemoryCacheModule>();

            builder.RegisterModule<SecurityModule>();

            builder.RegisterModule<DomainServiceModule>();

            builder.RegisterModule<ApplicationModule>();

            builder.AddMediatR(new[] {
                typeof(Program).Assembly
            });

            //builder.AddRabbitMq(Configuration);
            //builder.RegisterType<RabbitMqDomainEventProvider>().AsSelf().AsImplementedInterfaces();
        }

        public void Configure(IApplicationBuilder app,
                              IWebHostEnvironment env,
                              IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("GPNSM.Blank.API");

            app.UseStaticFiles();
            app.UseResponseCompression();

            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    // Need create client of keycloak for service and set redirect valid and enable Implicit flow
                    options.OAuth2RedirectUrl($"{Configuration.GetValue<string>("BaseUrl") ?? "http://localhost"}/swagger/oauth2-redirect.html");
                    options.OAuthClientId(Configuration.GetValue<string>(Consts.Swagger.KeyName));
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint(
                            $"/swagger/{description.GroupName}/swagger.json",
                            description.GroupName.ToUpperInvariant());
                    }
                });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
