﻿using AutoMapper;
using GPNSM.Blank.Application.Commands.Blanks;
using GPNSM.Blank.Application.Commands.Blanks.BlankItems;
using GPNSM.Blank.Application.Queries.Blanks;
using GPNSM.Blank.Application.ViewModels.Blank;
using GPNSM.Blank.Application.ViewModels.Blank.BlankItem;
using GPNSM.Blank.Persistence.Entities;
using GPNSM.Blank.Persistence.Entities.Dictionaries;
using GPNSM.Blank.Persistence.Filters;

namespace GPNSM.Blank.Application.Mapper
{
    public class ApplicationProfiler : Profile
    {
        public ApplicationProfiler()
        {
            CreateMap<BlankEntity, BlankViewModel>();
            CreateMap<BlankViewModel, BlankEntity>();

            CreateMap<BlankEntity, BlankDto>()
                .ForMember(m => m.FullName, e => e.MapFrom(m => $"{m.FirstName} {m.SecondName}"));

            CreateMap<CreateBlankCommand, Domain.Aggregats.Blanks.Blank>();
            CreateMap<UpdateBlankCommand, Domain.Aggregats.Blanks.Blank>();

            CreateMap<Domain.Aggregats.Blanks.Blank, BlankViewModel>();

            CreateMap<BlankType, BlankTypeView>();
            CreateMap<BlankTypeView, BlankType>();

            CreateMap<GetBlanksRequest, BlankFilter>();

            CreateMap<BlankItemEntity, BlankItemDto>()
            .ForMember(m => m.Description, e => e.MapFrom(m => $"DTO: {m.Description}"));

            CreateMap<Domain.Aggregats.Blanks.Blank.BlankItem, BlankItemViewModel>();

            CreateMap<CreateBlankItemCommand, Domain.Aggregats.Blanks.Blank>()
                .ForMember(m => m.Id, e => e.MapFrom(m => m.BlankId));
            CreateMap<CreateBlankItemCommand, Domain.Aggregats.Blanks.Blank.BlankItem>();
            CreateMap<UpdateBlankItemCommand, Domain.Aggregats.Blanks.Blank>();
            CreateMap<UpdateBlankItemCommand, Domain.Aggregats.Blanks.Blank.BlankItem>();
        }
    }
}
