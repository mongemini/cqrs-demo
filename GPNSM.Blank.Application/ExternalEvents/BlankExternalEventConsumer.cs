﻿using GPNSM.Logger;
using GPNSM.Shared.ExternalEvent;
using MassTransit;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.ExternalEvents
{
    public class BlankExternalEventConsumer : IConsumer<BlankExternalDomainEvent>
    {
        private readonly ILog _logger;
        public BlankExternalEventConsumer(ILog logger)
        {
            _logger = logger;
        }

        public Task Consume(ConsumeContext<BlankExternalDomainEvent> context)
        {
            _logger.Info("BlankExternalEventConsumer -> Consume");
            return Task.CompletedTask;
        }
    }
}
