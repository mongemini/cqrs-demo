﻿using GPNSM.Logger;
using GPNSM.Shared.ExternalEvent;
using MassTransit;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.ExternalEvents
{
    public class CreatedUserEventConsumer : IConsumer<CreatedUserSharedData>
    {
        private readonly ILog _logger;
        public CreatedUserEventConsumer(ILog logger)
        {
            _logger = logger;
        }

        public Task Consume(ConsumeContext<CreatedUserSharedData> context)
        {
            _logger.Info($"CreatedUserEventConsumer -> id {context.Message.Id}");
            return Task.CompletedTask;
        }
    }
}
