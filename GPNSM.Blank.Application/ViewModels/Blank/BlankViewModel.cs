﻿using GPNSM.Application.Core.ViewModels;
using System;

namespace GPNSM.Blank.Application.ViewModels.Blank
{
    public class BlankViewModel: BaseViewModel
    {
        public string FirstName { get; set; }

        public string SecondName { get; set; }
    }
}
