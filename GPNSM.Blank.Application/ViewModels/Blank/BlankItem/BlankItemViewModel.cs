﻿using GPNSM.Application.Core.ViewModels;

namespace GPNSM.Blank.Application.ViewModels.Blank.BlankItem
{
    public class BlankItemViewModel: BaseViewModel
    {
        public string Description { get; set; }
    }
}
