﻿using GPNSM.Application.Core.ViewModels;

namespace GPNSM.Blank.Application.ViewModels.Blank
{
    public class BlankDto: BaseViewModel
    {
        public string FullName { get; set; }
    }
}
