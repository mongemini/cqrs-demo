﻿using GPNSM.Application.Core.Queries;
using GPNSM.Blank.Application.ViewModels.Blank;

namespace GPNSM.Blank.Application.Queries.Blanks
{
    public class GetBlankTypesRequest : CachedRequest<BlankTypeView[]>
    {
        public GetBlankTypesRequest() : base(Consts.CacheKeys.Dictionaries.BlankTypes) { }
    }
}
