﻿using AutoMapper;
using GPNSM.Application.Core.Queries;
using GPNSM.Application.Core.ViewModels;
using GPNSM.Blank.Application.ViewModels.Blank;
using GPNSM.Blank.Persistence.Entities;
using GPNSM.Blank.Persistence.Filters;
using System.Threading;
using System.Threading.Tasks;
using GPNSM.Application.Core.Common;
using GPNSM.Blank.Persistence.Repositories.Contracts;

namespace GPNSM.Blank.Application.Queries.Blanks
{
    public class GetBlanksHandler : PagedRequestHandler<GetBlanksRequest, BlankDto>
    {
        private readonly IMapper _mapper;
        private readonly IBlankRepository _blankRepository;
        public GetBlanksHandler(IMapper mapper, IBlankRepository blankRepository)
        {
            _mapper = mapper;
            _blankRepository = blankRepository;
        }

        public override Task<PagedList<BlankDto>> Handle(GetBlanksRequest request, CancellationToken cancellationToken)
        {
            var filter = _mapper.Map<BlankFilter>(request);
            return _blankRepository.Where(filter.Build()).ToLookupAsync<BlankEntity, BlankDto>(filter, _mapper, cancellationToken);
        }
    }
}
