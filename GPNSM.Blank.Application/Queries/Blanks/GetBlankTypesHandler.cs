﻿using AutoMapper;
using GPNSM.Application.Core.Queries;
using GPNSM.Blank.Application.ViewModels.Blank;
using GPNSM.Blank.Persistence.Entities.Dictionaries;
using GPNSM.Blank.Persistence.Repositories.Dictionaries;
using GPNSM.Common.Core.Extensions;
using Microsoft.Extensions.Caching.Distributed;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Queries.Blanks
{
    public class GetBlankTypesHandler : CachedHandler<GetBlankTypesRequest, BlankTypeView[]>
    {
        private readonly IBlankTypeRepository _blankTypeRepository;
        private readonly IMapper _mapper;
        public GetBlankTypesHandler(IBlankTypeRepository blankTypeRepository, 
            IDistributedCache cache,
            IMapper mapper) 
            : base(cache)
        {
            _blankTypeRepository = blankTypeRepository;
            _mapper = mapper;
        }
       
        protected override Task<BlankTypeView[]> HandleAsync(GetBlankTypesRequest request, CancellationToken cancellationToken)
        {
            return _blankTypeRepository.GetAll().ToArrayAsync<BlankType, BlankTypeView>(_mapper, cancellationToken);
        }
    }
}
