﻿using AutoMapper;
using GPNSM.Blank.Application.ViewModels.Blank;
using GPNSM.Blank.Persistence.Entities;
using GPNSM.Blank.Persistence.Repositories.Contracts;
using GPNSM.Common.Core.Extensions;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Queries.Blanks
{

    public class GetBlankHandler : IRequestHandler<GetBlankRequest, BlankDto>
    {
        private readonly IMapper _mapper;
        private readonly IBlankRepository _blankRepository;

        public GetBlankHandler(IMapper mapper, IBlankRepository blankRepository)
        {
            _mapper = mapper;
            _blankRepository = blankRepository;
        }

        public async Task<BlankDto> Handle(GetBlankRequest request, CancellationToken cancellationToken)
        {
            var blank = await _blankRepository.Get(request.Id).SingleOrDefaultAsync<BlankEntity, BlankDto>(_mapper, cancellationToken);
            if (blank == null)
                throw new Exception("blank not found");
            return blank;
        }
    }
}
