﻿using GPNSM.Blank.Application.ViewModels.Blank;
using MediatR;
using System;

namespace GPNSM.Blank.Application.Queries.Blanks
{
    public class GetBlankRequest : IRequest<BlankDto>
    {
        public GetBlankRequest(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; private set; }
    }
}
