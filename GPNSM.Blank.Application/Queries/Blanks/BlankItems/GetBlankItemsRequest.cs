﻿using GPNSM.Blank.Application.ViewModels.Blank.BlankItem;
using MediatR;
using System;

namespace GPNSM.Blank.Application.Queries.Blanks.BlankItems
{
    public class GetBlankItemsRequest : IRequest<BlankItemDto[]>
    {
        public GetBlankItemsRequest(Guid blankId)
        {
            BlankId = blankId;
        }
        public Guid BlankId { get; private set; }
    }
}
