﻿using GPNSM.Blank.Application.ViewModels.Blank.BlankItem;
using MediatR;
using System;

namespace GPNSM.Blank.Application.Queries.Blanks.BlankItems
{
    public class GetBlankItemRequest : IRequest<BlankItemDto>
    {
        public GetBlankItemRequest(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; private set; }
    }
}
