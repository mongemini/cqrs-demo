﻿using AutoMapper;
using GPNSM.Blank.Persistence.Criterias;
using GPNSM.Blank.Persistence.Entities;
using GPNSM.Blank.Persistence.Repositories.Contracts;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using GPNSM.Blank.Application.ViewModels.Blank.BlankItem;
using GPNSM.Common.Core.Extensions;

namespace GPNSM.Blank.Application.Queries.Blanks.BlankItems
{
    public class GetBlankItemsHandler : IRequestHandler<GetBlankItemsRequest, BlankItemDto[]>
    {
        private readonly IMapper _mapper;
        private readonly IBlankItemRepository _blankItemRepository;

        public GetBlankItemsHandler(IMapper mapper, IBlankItemRepository blankItemRepository)
        {
            _mapper = mapper;
            _blankItemRepository = blankItemRepository;
        }

        public Task<BlankItemDto[]> Handle(GetBlankItemsRequest request, CancellationToken cancellationToken)
        {
            return _blankItemRepository.Where(new BlankItemsByBlankId(request.BlankId).Build()).ToArrayAsync<BlankItemEntity, BlankItemDto>(_mapper, cancellationToken);
        }
    }
}
