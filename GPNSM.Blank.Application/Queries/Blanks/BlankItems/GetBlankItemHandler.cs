﻿using AutoMapper;
using GPNSM.Blank.Application.ViewModels.Blank.BlankItem;
using GPNSM.Blank.Persistence.Entities;
using GPNSM.Blank.Persistence.Repositories.Contracts;
using GPNSM.Common.Core.Extensions;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Queries.Blanks.BlankItems
{
    public class GetBlankItemHandler : IRequestHandler<GetBlankItemRequest, BlankItemDto>
    {
        private readonly IMapper _mapper;
        private readonly IBlankItemRepository _blankItemRepository;

        public GetBlankItemHandler(IMapper mapper, IBlankItemRepository blankItemRepository)
        {
            _mapper = mapper;
            _blankItemRepository = blankItemRepository;
        }

        public async Task<BlankItemDto> Handle(GetBlankItemRequest request, CancellationToken cancellationToken)
        {
            var blankItem = await _blankItemRepository.Get(request.Id).SingleOrDefaultAsync<BlankItemEntity, BlankItemDto>(_mapper, cancellationToken);
            if (blankItem == null)
                throw new Exception("blank item not found");
            return blankItem;
        }
    }
}