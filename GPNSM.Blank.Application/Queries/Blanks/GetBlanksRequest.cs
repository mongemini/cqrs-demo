﻿using GPNSM.Application.Core.Queries;
using GPNSM.Blank.Application.ViewModels.Blank;
using System;

namespace GPNSM.Blank.Application.Queries.Blanks
{
    public class GetBlanksRequest : PagedRequest<BlankDto>
    {
        public Guid? CreatedBy { get; set; }
    }
}
