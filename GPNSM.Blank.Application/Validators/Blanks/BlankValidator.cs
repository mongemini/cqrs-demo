﻿using FluentValidation;
using GPNSM.Blank.Application.Commands.Blanks;
using System;
using GPNSM.Blank.Application.Resources;

namespace GPNSM.Blank.Application.Validators.Blanks
{
    class BlankValidator : AbstractValidator<CreateBlankCommand>
    {
        public BlankValidator()
        {
            RuleFor(m => m.FirstName).Cascade(CascadeMode.StopOnFirstFailure)
               .NotEmpty().WithMessage(x => String.Format(ValidationErrorMessage.IsRequired, nameof(x.FirstName)))
               .MaximumLength(50).WithMessage(x => String.Format(ValidationErrorMessage.MaxLength, nameof(x.FirstName), 50));

            RuleFor(m => m.SecondName).Cascade(CascadeMode.StopOnFirstFailure)
               .NotEmpty().WithMessage(x => String.Format(ValidationErrorMessage.IsRequired, nameof(x.SecondName)))
               .MaximumLength(50).WithMessage(x => String.Format(ValidationErrorMessage.MaxLength, nameof(x.SecondName), 50));
        }
    }
}
