﻿using FluentValidation;
using GPNSM.Blank.Application.Commands.Blanks;

namespace GPNSM.Blank.Application.Validators.Blanks
{
    public class CreateBlankValidator: AbstractValidator<CreateBlankCommand>
    {
        public CreateBlankValidator()
        {
            RuleFor(m => m).SetValidator(new BlankValidator());
        }
    }
}
