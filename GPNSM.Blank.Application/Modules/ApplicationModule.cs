﻿using Autofac;
using GPNSM.Application.Core.Modules;

namespace GPNSM.Blank.Application.Modules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<CoreBehaviorModule>();
            builder.RegisterModule<NotificationModule>();
            builder.RegisterModule<RequestModule>();
            builder.RegisterModule<ValidatorModule>();
            builder.RegisterModule<ConsumerModule>();
            builder.RegisterModule<ApplicationServiceModule>();
            base.Load(builder);
        }
    }
}
