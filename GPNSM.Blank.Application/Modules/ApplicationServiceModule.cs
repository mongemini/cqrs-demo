﻿using Autofac;
using GPNSM.Application.Core.Notification;
using GPNSM.Blank.Application.Services;

namespace GPNSM.Blank.Application.Modules
{
    public class ApplicationServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SomeApplicationService>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<MediatREventDispatcher>().AsSelf().AsImplementedInterfaces();
            base.Load(builder);
        }
    }
}
