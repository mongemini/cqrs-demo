﻿using Autofac;
using MassTransit;

namespace GPNSM.Blank.Application.Modules
{
    public class ConsumerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterConsumers(ThisAssembly);

            base.Load(builder);
        }
    }
}
