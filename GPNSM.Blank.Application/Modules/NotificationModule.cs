﻿using Autofac;
using MediatR;
using System;
using System.Linq;

namespace GPNSM.Blank.Application.Modules
{
    public class NotificationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var notifications = ThisAssembly.GetTypes()
                .Where(t => t is INotification)
                .ToArray();
            Array.ForEach(notifications, e => builder.RegisterType(e).AsSelf().AsImplementedInterfaces());

            var notificationHandlers = ThisAssembly.GetTypes()
               .Where(t => t.IsClosedTypeOf(typeof(INotificationHandler<>)))
               .ToArray();
            Array.ForEach(notificationHandlers, e => builder.RegisterType(e).AsSelf().AsImplementedInterfaces());
            base.Load(builder);
        }
    }
}
