﻿using Autofac;
using FluentValidation;
using System;
using System.Linq;

namespace GPNSM.Blank.Application.Modules
{
    public class ValidatorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var validators = ThisAssembly.GetTypes().Where(t => t.IsClosedTypeOf(typeof(IValidator<>))).ToArray();
            Array.ForEach(validators, v => builder.RegisterType(v).AsSelf().AsImplementedInterfaces());
            base.Load(builder);
        }
    }
}
