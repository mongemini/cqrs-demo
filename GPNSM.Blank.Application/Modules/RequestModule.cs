﻿using Autofac;
using System;
using System.Linq;
using MediatR;

namespace GPNSM.Blank.Application.Modules
{
    public class RequestModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var requests = ThisAssembly.GetTypes()
                .Where(t => t.IsClosedTypeOf(typeof(IRequest<>)))
                .ToArray();
            Array.ForEach(requests, e => builder.RegisterType(e).AsSelf().AsImplementedInterfaces());

            var handlers = ThisAssembly.GetTypes()
               .Where(t => t.IsClosedTypeOf(typeof(IRequestHandler<,>)))
               .ToArray();
            Array.ForEach(handlers, e => builder.RegisterType(e).AsSelf().AsImplementedInterfaces());
            base.Load(builder);
        }
    }
}
