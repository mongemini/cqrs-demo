﻿using GPNSM.Logger;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.InternalEvents
{
    public class BlankSomeHappenedDomainEventHandler : INotificationHandler<BlankSomeHappenedDomainEvent>
    {
        private readonly ILog _logger;
        public BlankSomeHappenedDomainEventHandler(ILog logger)
        {

        }

        public Task Handle(BlankSomeHappenedDomainEvent notification, CancellationToken cancellationToken)
        {
            //_logger.Info("BlankSomeHappenedDomainEventHandler -> Handle");
            return Task.CompletedTask;
        }
    }
}
