﻿using GPNSM.Application.Core.Notification;
using GPNSM.Domain.Core;
using GPNSM.Shared.ExternalEvent;
using System;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.InternalEvents
{
    public class BlankSomeHappenedDomainEvent: MediatRDomainEvent
    {
        public BlankSomeHappenedDomainEvent() : base(true) { }

        public Guid HappendId { get; set; }

        public override async Task CallExternalEvent(IExternalDomainEventProvider eventProvider)
        {
            await eventProvider.PublishAsync(new BlankExternalDomainEvent());
        }
    }
}
