﻿using GPNSM.Application.Core.Commands;
using MediatR;
using System;

namespace GPNSM.Blank.Application.Commands.Blanks
{
    public class DeleteBlankCommand : BaseDeleteCommand, IRequest<bool>
    {
        public DeleteBlankCommand(Guid id) : base(id) { }
    }
}
