﻿using GPNSM.Application.Core.Commands;
using GPNSM.Blank.Application.ViewModels.Blank.BlankItem;
using MediatR;
using System;

namespace GPNSM.Blank.Application.Commands.Blanks.BlankItems
{
    public class CreateBlankItemCommand : BaseCreateCommand, IRequest<BlankItemViewModel>
    {
        public Guid BlankId { get; set; }
        public string Description { get; set; }
    }
}
