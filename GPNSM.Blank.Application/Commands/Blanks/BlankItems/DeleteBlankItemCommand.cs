﻿using GPNSM.Application.Core.Commands;
using MediatR;
using System;

namespace GPNSM.Blank.Application.Commands.Blanks.BlankItems
{
    public class DeleteBlankItemCommand : BaseDeleteCommand, IRequest<bool>
    {
        public DeleteBlankItemCommand(Guid id) : base(id) { }
    }
}
