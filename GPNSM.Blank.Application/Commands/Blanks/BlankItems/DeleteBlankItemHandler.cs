﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Commands.Blanks.BlankItems
{
    public class DeleteBlankItemHandler : IRequestHandler<DeleteBlankItemCommand, bool>
    {
        private readonly Domain.Aggregats.Blanks.Blank _blank;

        public DeleteBlankItemHandler(Domain.Aggregats.Blanks.Blank aggregate)
        {
            _blank = aggregate;
        }

        public async Task<bool> Handle(DeleteBlankItemCommand request, CancellationToken cancellationToken)
        {
            return await _blank.DeleteBlankItemAsync(request.Id, cancellationToken);
        }
    }
}
