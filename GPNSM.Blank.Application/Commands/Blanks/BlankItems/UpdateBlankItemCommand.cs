﻿using GPNSM.Application.Core.Commands;
using GPNSM.Blank.Application.ViewModels.Blank.BlankItem;
using MediatR;

namespace GPNSM.Blank.Application.Commands.Blanks.BlankItems
{
    public class UpdateBlankItemCommand : BaseUpdateCommand, IRequest<BlankItemViewModel>
    {
        public string Description { get; set; }
    }
}
