﻿using AutoMapper;
using GPNSM.Application.Core.Commands;
using GPNSM.Blank.Application.ViewModels.Blank.BlankItem;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Commands.Blanks.BlankItems
{
    public class UpdateBlankItemHandler : RequestHandlerBase<UpdateBlankItemCommand, Domain.Aggregats.Blanks.Blank, BlankItemViewModel, Domain.Aggregats.Blanks.Blank.BlankItem>
    {
        private readonly IMapper _mapper;
        private readonly Domain.Aggregats.Blanks.Blank _blank;

        public UpdateBlankItemHandler(IMapper mapper,
                                      Domain.Aggregats.Blanks.Blank aggregate)
                                      : base(mapper, aggregate)
        {
            _mapper = mapper;
            _blank = aggregate;
        }

        protected override Task<Domain.Aggregats.Blanks.Blank.BlankItem> HandleInternal(Domain.Aggregats.Blanks.Blank request, CancellationToken cancellationToken)
        {
            return _blank.UpdateBlankItemAsync(_mapper.Map<Domain.Aggregats.Blanks.Blank.BlankItem>(_request), cancellationToken);
        }
    }
}
