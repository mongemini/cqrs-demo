﻿using GPNSM.Application.Core.Commands;
using GPNSM.Blank.Application.ViewModels.Blank;
using MediatR;

namespace GPNSM.Blank.Application.Commands.Blanks
{
    public class CreateBlankCommand : BaseCreateCommand, IRequest<BlankViewModel>
    {
        //public Guid? CreatedBy { get; set; }
        public string FirstName { get; set; }

        public string SecondName { get; set; }
    }
}
