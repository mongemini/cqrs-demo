﻿using AutoMapper;
using GPNSM.Application.Core.Commands;
using GPNSM.Blank.Application.InternalEvents;
using GPNSM.Blank.Application.ViewModels.Blank;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Commands.Blanks
{
    public class CreateBlankHandler : RequestHandlerBase<CreateBlankCommand, Domain.Aggregats.Blanks.Blank, BlankViewModel, Domain.Aggregats.Blanks.Blank>
    {
        private readonly IMapper _mapper;
        private readonly Domain.Aggregats.Blanks.Blank _blank;

        public CreateBlankHandler(IMapper mapper,
                                  Domain.Aggregats.Blanks.Blank aggregate)
                                  : base(mapper, aggregate)
        {
            _mapper = mapper;
            _blank = aggregate;
        }

        protected async override Task<Domain.Aggregats.Blanks.Blank> HandleInternal(Domain.Aggregats.Blanks.Blank request, CancellationToken cancellationToken)
        {
            _blank.AddDomainEvent(new BlankSomeHappenedDomainEvent());
            return await _blank.CreateBlankAsync(_blank, cancellationToken);
        }
    }
}
