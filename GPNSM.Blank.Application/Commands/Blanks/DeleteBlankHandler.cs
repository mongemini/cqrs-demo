﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Commands.Blanks
{
    public class DeleteBlankHandler : IRequestHandler<DeleteBlankCommand, bool>
    {
        private readonly Domain.Aggregats.Blanks.Blank _blank;

        public DeleteBlankHandler(Domain.Aggregats.Blanks.Blank aggregate)
        {
            _blank = aggregate;
        }

        public async Task<bool> Handle(DeleteBlankCommand request, CancellationToken cancellationToken)
        {
            return await _blank.DeleteBlankAsync(request.Id, cancellationToken);
        }
    }
}
