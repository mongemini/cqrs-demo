﻿using GPNSM.Application.Core.Commands;
using GPNSM.Blank.Application.ViewModels.Blank;
using MediatR;
using System;

namespace GPNSM.Blank.Application.Commands.Blanks
{
    public class UpdateBlankCommand : BaseUpdateCommand, IRequest<BlankViewModel>
    {
        // public Guid? UpdatedBy { get; set; }
        public string FirstName { get; set; }

        public string SecondName { get; set; }
    }
}
