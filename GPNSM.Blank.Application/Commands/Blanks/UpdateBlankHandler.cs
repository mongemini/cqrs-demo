﻿using AutoMapper;
using GPNSM.Application.Core.Commands;
using GPNSM.Blank.Application.ViewModels.Blank;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Commands.Blanks
{
    public class UpdateBlankHandler : RequestHandlerBase<UpdateBlankCommand, Domain.Aggregats.Blanks.Blank, BlankViewModel, Domain.Aggregats.Blanks.Blank>
    {
        private readonly IMapper _mapper;
        private readonly Domain.Aggregats.Blanks.Blank _blank;

        public UpdateBlankHandler(IMapper mapper,
                                  Domain.Aggregats.Blanks.Blank aggregate)
                                  : base(mapper, aggregate)
        {
            _mapper = mapper;
            _blank = aggregate;
        }

        protected override Task<Domain.Aggregats.Blanks.Blank> HandleInternal(Domain.Aggregats.Blanks.Blank request, CancellationToken cancellationToken)
        {
            return _blank.UpdateBlankAsync(_blank, cancellationToken);
        }
    }
}
