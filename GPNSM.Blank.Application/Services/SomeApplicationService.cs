﻿using GPNSM.Blank.Application.Services.Contracts;
using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Services
{
    public class SomeApplicationService : ISomeApplicationService
    {
        public Task<string> SomeDoAsync()
        {
            return Task.FromResult("SomeApplicationService -> SomeDoAsync");
        }
    }
}
