﻿using System.Threading.Tasks;

namespace GPNSM.Blank.Application.Services.Contracts
{
    public interface ISomeApplicationService
    {
        Task<string> SomeDoAsync();
    }
}
