﻿using AutoMapper;
using GPNSM.Application.Core.ViewModels;
using GPNSM.Common.Core.Extensions;
using GPNSM.Data.Core.Entities;
using GPNSM.Repositories.Core.Contracts;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Application.Core.Queries
{
    public class DictionaryRequestHandlerBase<TRequest, TResponseItem, TDictionaryEntity> : CachedHandler<TRequest, TResponseItem[]>
        where TDictionaryEntity : DictionaryEntity
        where TRequest : IRequest<TResponseItem[]>, ICachedRequest
        where TResponseItem: DictionaryView
    {
        private readonly IDictionaryRepository<TDictionaryEntity> _dictionaryRepository;
        private readonly IMapper _mapper;

        public DictionaryRequestHandlerBase(IDictionaryRepository<TDictionaryEntity> dictionaryRepository,
            IDistributedCache cache,
            IMapper mapper)
            : base(cache)
        {
            _dictionaryRepository = dictionaryRepository;
            _mapper = mapper;
        }

        protected override Task<TResponseItem[]> HandleAsync(TRequest request, CancellationToken cancellationToken)
        {
            return _dictionaryRepository.GetAll().ToArrayAsync<TDictionaryEntity, TResponseItem>(_mapper, cancellationToken);
        }
    }
}
