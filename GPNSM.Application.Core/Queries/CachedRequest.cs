﻿using MediatR;

namespace GPNSM.Application.Core.Queries
{
    public abstract class CachedRequest<TResult> : IRequest<TResult>, ICachedRequest
    {
        public CachedRequest(string cacheKey)
        {
            CacheKey = cacheKey;
        }
        public virtual string CacheKey { get; }
    }
}
