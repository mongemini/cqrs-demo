﻿using GPNSM.Application.Core.Contracts;
using GPNSM.Application.Core.ViewModels;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Application.Core.Queries
{
    public abstract class PagedRequestHandler<TRequest, TResult> : IRequestHandler<TRequest, PagedList<TResult>>
        where TRequest : IRequest<PagedList<TResult>>, IPagedRequest
    {

        public abstract Task<PagedList<TResult>> Handle(TRequest request, CancellationToken cancellationToken);
    }
}
