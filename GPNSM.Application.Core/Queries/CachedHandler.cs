﻿using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using System.Threading;
using System.Threading.Tasks;
using GPNSM.Caching.Utilities;

namespace GPNSM.Application.Core.Queries
{
    public abstract class CachedHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse> 
        where TRequest : IRequest<TResponse>, ICachedRequest
    {
        protected CachedHandler(IDistributedCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        private readonly IDistributedCache _memoryCache;

        protected abstract Task<TResponse> HandleAsync(TRequest request, CancellationToken cancellationToken);

        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken)
        {
            return _memoryCache.GetOrCreateAsync(request.CacheKey, () => HandleAsync(request, cancellationToken), cancellationToken);
        }
    }
}
