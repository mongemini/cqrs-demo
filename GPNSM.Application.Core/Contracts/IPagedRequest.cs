﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Application.Core.Contracts
{
    public interface IPagedRequest
    {
        string Sort { get; }
        int Direction { get; }

        int? Page { get; }
        int? Size { get; }
    }
}
