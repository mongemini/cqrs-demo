﻿namespace GPNSM.Application.Core.Queries
{
    public interface ICachedRequest
    {
        string CacheKey { get; }
    }
}
