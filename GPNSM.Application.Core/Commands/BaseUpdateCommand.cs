﻿using System;

namespace GPNSM.Application.Core.Commands
{
    public abstract class BaseUpdateCommand: BaseCommand
    {
        public Guid Id { get; set; }
    }
}
