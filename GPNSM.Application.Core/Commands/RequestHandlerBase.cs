﻿using AutoMapper;
using GPNSM.Domain.Core;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Application.Core.Commands
{
    public abstract class RequestHandlerBase<TRequest, TAggregateRoot, TResponse, TEntityDomain> : IRequestHandler<TRequest, TResponse>
          where TRequest : IRequest<TResponse>
          where TAggregateRoot : AggregateRoot
          where TEntityDomain : BaseEntityDomain<Guid>
    {
        private readonly IMapper _mapper;
        private readonly TAggregateRoot _aggregate;
        protected TRequest _request;

        public RequestHandlerBase(IMapper mapper, TAggregateRoot aggregate)
        {
            _mapper = mapper;
            _aggregate = aggregate;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken)
        {
            _request = request;
            _mapper.Map(request, _aggregate);
            var result = await HandleInternal(_aggregate, cancellationToken);

            return _mapper.Map<TResponse>(result);
        }

        protected abstract Task<TEntityDomain> HandleInternal(TAggregateRoot request, CancellationToken cancellationToken);
    }

    public abstract class RequestHandlerBase<TRequest, TAggregateRoot, TResponse> : IRequestHandler<TRequest, TResponse>
         where TRequest : IRequest<TResponse>
         where TAggregateRoot : BaseEntityDomain<Guid>
    {
        private readonly IMapper _mapper;
        private readonly TAggregateRoot _aggregate;

        public RequestHandlerBase(IMapper mapper, TAggregateRoot aggregate)
        {
            _mapper = mapper;
            _aggregate = aggregate;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken)
        {
            _mapper.Map(request, _aggregate);
            var result = await HandleInternal(_aggregate, cancellationToken);
            return _mapper.Map<TResponse>(result);

        }

        protected abstract Task<TAggregateRoot> HandleInternal(TAggregateRoot request, CancellationToken cancellationToken);
    }
}
