﻿using System;

namespace GPNSM.Application.Core.Commands
{
    public abstract class BaseDeleteCommand
    {
        public BaseDeleteCommand(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
