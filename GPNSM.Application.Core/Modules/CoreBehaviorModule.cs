﻿using Autofac;
using GPNSM.Application.Core.Behaviors;
using MediatR;

namespace GPNSM.Application.Core.Modules
{
    public class CoreBehaviorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(ExceptionBehavior<,>)).As(typeof(IPipelineBehavior<,>));
            builder.RegisterGeneric(typeof(ValidatorBehavior<,>)).As(typeof(IPipelineBehavior<,>));
            base.Load(builder);
        }
    }
}
