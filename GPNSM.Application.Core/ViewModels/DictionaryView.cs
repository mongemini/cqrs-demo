﻿namespace GPNSM.Application.Core.ViewModels
{
    public class DictionaryView : BaseView<int>
    {
        public string Value { get; set; }
    }
}
