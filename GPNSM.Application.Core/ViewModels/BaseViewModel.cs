﻿using System;

namespace GPNSM.Application.Core.ViewModels
{
    public abstract class BaseViewModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Guid UpdatedBy { get; set; }
}
}
