﻿using System;

namespace GPNSM.Application.Core.ViewModels
{
    public abstract class BaseView<TKey>
    {
        public TKey Id { get; set; }
    }

    public abstract class BaseView : BaseView<Guid>
    {
        public DateTime CreatedDate { get; set; }
    }
}
