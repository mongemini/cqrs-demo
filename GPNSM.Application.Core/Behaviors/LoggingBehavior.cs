﻿using GPNSM.Logger;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Application.Core.Behaviors
{
    //public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    //{
    //    private readonly ILogger _logger;
    //    public LoggingBehavior(ILogger logger) => _logger = logger;

    //    public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
    //    {
    //        _logger.Info($"Handling {typeof(TRequest).Name}");
    //        var response = await next();
    //        _logger.Info($"Handled {typeof(TResponse).Name}");
    //        return response;
    //    }
    //}
}
