﻿using FluentValidation;
using GPNSM.Domain.Core.Exceptions;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GPNSM.Application.Core.Behaviors
{
    public class ExceptionBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        public ExceptionBehavior() { }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            try
            {
                var response = await next();
                return response;
            }
            catch (GPNSM.Domain.Core.Exceptions.NotFoundException ex)
            {
                throw new GPNSM.Application.Core.Exceptions.NotFoundException("Not found exception", ex);
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (GPNSM.Application.Core.Exceptions.ApplicationException)
            {
                throw;
            }
            catch (Exception ex) 
            {
                throw new GPNSM.Application.Core.Exceptions.ApplicationException("Application layer exeption", ex);
            }
        }
    }
}
