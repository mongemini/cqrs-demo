﻿using GPNSM.Domain.Core;
using MediatR;
using System.Threading.Tasks;

namespace GPNSM.Application.Core.Notification
{
    public class MediatREventDispatcher: IDomainEventDispatcher
    {
        private readonly IMediator _mediator;
        private readonly IExternalDomainEventProvider _eventProvider;

        public MediatREventDispatcher(IMediator mediator, IExternalDomainEventProvider eventProvider)
        {
            _mediator = mediator;
            _eventProvider = eventProvider;
        }

        public async Task Raise(AggregateRoot aggregate)
        {
           await _mediator.DispatchDomainEventsAsync(aggregate, _eventProvider);
        }
    }
}
