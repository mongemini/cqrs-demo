﻿using GPNSM.Domain.Core;
using MediatR;
using System.Linq;
using System.Threading.Tasks;

namespace GPNSM.Application.Core.Notification
{
    public static class MediatorExtension
    {
        public static async Task DispatchDomainEventsAsync(
                    this IMediator mediator, 
                    AggregateRoot aggregate,
                    IExternalDomainEventProvider eventProvider)
        {
            var domainEvents = aggregate.DomainEvents;

            var tasks = domainEvents
                .Select(async (domainEvent) => {

                    await mediator.Publish(domainEvent);

                    if (domainEvent.IsExternal)
                    {
                        //TODO: переделать на дженерики и передавать само сообщение в провайдет
                        await domainEvent.CallExternalEvent(eventProvider);
                    }
                });

            await Task.WhenAll(tasks).ContinueWith(_ => aggregate.ClearEvents());
        }
    }
}
