﻿using GPNSM.Domain.Core;
using MediatR;

namespace GPNSM.Application.Core.Notification
{
    public abstract class MediatRDomainEvent: DomainEvent, INotification
    {
        public MediatRDomainEvent(bool IsExternal):
            base(IsExternal)
        {
        }
    }
}
