﻿using GPNSM.Common.Core.Exceptions;
using System;

namespace GPNSM.Application.Core.Exceptions
{
    public class ApplicationException : BaseException
    {
        public ApplicationException() : base() { }
        public ApplicationException(string message) : base(message) { }
        public ApplicationException(string message, Exception innerException) : base(message, innerException) { }
        protected ApplicationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
