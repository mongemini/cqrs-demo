﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Application.Core.Exceptions
{
    public class NotFoundException : ApplicationException
    {
        public NotFoundException() : this(string.Empty) { }
        public NotFoundException(string message) : base(message) { }
        public NotFoundException(string message, Exception inner) : base(message, inner) { }
        protected NotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
