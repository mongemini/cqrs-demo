﻿namespace GPNSM.Application.Core.Exceptions
{
    public class PermissionRequiredException : AccessDeniedException
    {
        public PermissionRequiredException(params object[] permission) : base()
        {
            //TODO: create view for permission
        }
        protected PermissionRequiredException(
      System.Runtime.Serialization.SerializationInfo info,
      System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
