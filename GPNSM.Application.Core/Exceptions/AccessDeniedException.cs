﻿using System;

namespace GPNSM.Application.Core.Exceptions
{
    public class AccessDeniedException : ApplicationException
    {
        public AccessDeniedException() : this(string.Empty) { }
        public AccessDeniedException(string message) : base(message) { }
        public AccessDeniedException(string message, Exception inner) : base(message, inner) { }
        protected AccessDeniedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
