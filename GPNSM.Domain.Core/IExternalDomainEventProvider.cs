﻿using System.Threading.Tasks;

namespace GPNSM.Domain.Core
{
    public interface IExternalDomainEventProvider
    {
        Task PublishAsync<T>(T externalEvent) where T : ExternalBaseDomainEvent;
    }
}
