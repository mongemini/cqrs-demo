﻿using System;
using System.Collections.Generic;

namespace GPNSM.Domain.Core
{
    public abstract class AggregateRoot : BaseEntityDomain<Guid>
    {

        protected readonly IDomainEventDispatcher _dispatcher;
        private List<DomainEvent> _domainEvents = new List<DomainEvent>();
        public virtual IReadOnlyList<DomainEvent> DomainEvents => _domainEvents;


        public AggregateRoot(IDomainEventDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }
        public virtual void AddDomainEvent(DomainEvent newEvent)
        {
            _domainEvents.Add(newEvent);
        }

        public virtual void ClearEvents()
        {
            _domainEvents.Clear();
        }
    }
}
