﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPNSM.Domain.Core.Modules
{
    public class DomainCoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var requests = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(c => c.FullName.StartsWith(nameof(GPNSM)))
                .SelectMany(assembly => assembly
                .GetTypes())
                .Where(type => type.IsClosedTypeOf(typeof(BaseEntityDomain<>)))
                .Where(type => !type.IsAbstract)
                .ToArray();

            builder.RegisterTypes(requests)
                .AsSelf()
                .AsImplementedInterfaces();

            base.Load(builder);
        }
    }
}
