﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Domain.Core
{
    public abstract class EntityDomain<TKey>
    {
        public virtual TKey Id { get; protected set; }

        public override bool Equals(object obj)
        {
            var other = obj as EntityDomain<TKey>;

            if (ReferenceEquals(other, null))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (GetType() != other.GetTypes())
                return false;

            //if (Id == 0 || other.Id == 0)
            //    return false;

            return Id.Equals(other.Id);
        }

        private Type GetTypes()
        {
            return this.GetType();
        }

        public static bool operator ==(EntityDomain<TKey> a, EntityDomain<TKey> b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(EntityDomain<TKey> a, EntityDomain<TKey> b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return (this.GetType().ToString() + Id).GetHashCode();
        }
    }

    public abstract class BaseEntityDomain<TKey> : EntityDomain<Guid>
    {
        public DateTime CreatedDate { get; set; }
        public TKey CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public TKey UpdatedBy
        {
            get; set;
        }
    }
}
