﻿using System.Threading.Tasks;

namespace GPNSM.Domain.Core
{
    public interface IDomainEventDispatcher
    {
        Task Raise(AggregateRoot aggregate);
    }
}
