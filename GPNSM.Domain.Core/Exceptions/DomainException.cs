﻿using GPNSM.Common.Core.Exceptions;
using System;

namespace GPNSM.Domain.Core.Exceptions
{
    public class DomainException : BaseException
    {
        public DomainException() : base() { }
        public DomainException(string message) : base(message) { }
        public DomainException(string message, Exception innerException) : base(message, innerException) { }
    }
}
