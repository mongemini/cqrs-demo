using System;

namespace GPNSM.Domain.Core.Exceptions
{
    public class NotFoundException : DomainException
    {
        public NotFoundException() : this(string.Empty) { }
        public NotFoundException(string message) : base(message) { }
        public NotFoundException(string message, Exception inner) : base(message, inner) { }
    }
}