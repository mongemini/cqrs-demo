﻿using GPNSM.Domain.Core;
using System;

namespace GPNSM.Shared.ExternalEvent.Distribution
{
    public class CreatedEmployeeExternalDomainEvent : ExternalBaseDomainEvent
    {
        public Guid Id { get; }

        public CreatedEmployeeExternalDomainEvent(Guid id) => Id = id;
    }
}
