﻿using GPNSM.Domain.Core;
using System;

namespace GPNSM.Shared.ExternalEvent
{
    public class CreatedUserSharedData: ExternalBaseDomainEvent
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int UserType { get; set; }
        public string JsonProperties { get; set; }
    }
}
