﻿using System;

namespace GPNSM.Common.Core.Exceptions
{
    public class BaseException : Exception
    {
        public BaseException() : base(string.Empty) { }
        public BaseException(string message) : base(message) { }
        public BaseException(string message, Exception inner) : base(message, inner) { }
        protected BaseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
