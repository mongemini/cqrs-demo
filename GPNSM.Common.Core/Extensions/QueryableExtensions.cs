﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace GPNSM.Common.Core.Extensions
{
    public static class QueryableExtensions
    {
        private static readonly FieldInfo _queryCompilerField = typeof(EntityQueryProvider).GetTypeInfo().DeclaredFields.Single(x => x.Name == "_queryCompiler");
        private static readonly TypeInfo _queryCompilerTypeInfo = typeof(QueryCompiler).GetTypeInfo();
        private static readonly FieldInfo _queryModelGeneratorField = _queryCompilerTypeInfo.DeclaredFields.Single(x => x.Name == "_queryModelGenerator");
        private static readonly FieldInfo _databaseField = _queryCompilerTypeInfo.DeclaredFields.Single(x => x.Name == "_database");
        private static readonly PropertyInfo _dependenciesProperty = typeof(Database).GetTypeInfo().DeclaredProperties.Single(x => x.Name == "Dependencies");

        public static TResult[] ToArray<TSource, TResult>(this IQueryable<TSource> source, IMapper mapper)
        {
            return source.ProjectTo<TResult>(mapper.ConfigurationProvider).ToArray();
        }

        public static Task<TResult[]> ToArrayAsync<TSource, TResult>(this IQueryable<TSource> source, IMapper mapper, CancellationToken cancellationToken)
        {
            return source.ProjectTo<TResult>(mapper.ConfigurationProvider).ToArrayAsync(cancellationToken);
        }

        public static TResult SingleOrDefault<TSource, TResult>(this IQueryable<TSource> source, IMapper mapper)
        {
            return source.ProjectTo<TResult>(mapper.ConfigurationProvider).SingleOrDefault();
        }

        public static Task<TResult> SingleOrDefaultAsync<TSource, TResult>(this IQueryable<TSource> source, IMapper mapper, CancellationToken cancellationToken)
        {
            return source.ProjectTo<TResult>(mapper.ConfigurationProvider).SingleOrDefaultAsync(cancellationToken);
        }

        public static TResult FirstOrDefault<TSource, TResult>(this IQueryable<TSource> source, IMapper mapper)
        {
            return source.ProjectTo<TResult>(mapper.ConfigurationProvider).FirstOrDefault();
        }

        public static Task<TResult> FirstOrDefaultAsync<TSource, TResult>(this IQueryable<TSource> source, IMapper mapper, CancellationToken cancellationToken)
        {
            return source.ProjectTo<TResult>(mapper.ConfigurationProvider).FirstOrDefaultAsync(cancellationToken);
        }

        public static Task<TResult> FirstOrDefaultAsync<TSource, TResult>(this IQueryable<TSource> source, IMapper mapper, Expression<Func<TResult, bool>> predicate, CancellationToken cancellationToken)
        {
            return source.ProjectTo<TResult>(mapper.ConfigurationProvider).FirstOrDefaultAsync(predicate, cancellationToken);
        }

        //public static string ToSql<TEntity>(this IQueryable<TEntity> queryable)
        //    where TEntity : class
        //{
        //    if (!(queryable is EntityQueryable<TEntity>) && !(queryable is InternalDbSet<TEntity>))
        //        throw new ArgumentException();

        //    var queryCompiler = (IQueryCompiler)_queryCompilerField.GetValue(queryable.Provider);
        //    var queryModelGenerator = (IQueryModelGenerator)_queryModelGeneratorField.GetValue(queryCompiler);
        //    var queryModel = queryModelGenerator.ParseQuery(queryable.Expression);
        //    var database = _databaseField.GetValue(queryCompiler);
        //    var queryCompilationContextFactory = ((DatabaseDependencies)_dependenciesProperty.GetValue(database)).QueryCompilationContextFactory;
        //    var queryCompilationContext = queryCompilationContextFactory.Create(false);
        //    var modelVisitor = (RelationalQueryModelVisitor)queryCompilationContext.CreateQueryModelVisitor();
        //    modelVisitor.CreateQueryExecutor<TEntity>(queryModel);
        //    return modelVisitor.Queries.Join(Environment.NewLine + Environment.NewLine);
        //}

        public static IOrderedQueryable<TSource> OrderByDirection<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> selector, bool descending)
        {
            return descending ? source.OrderByDescending(selector)
                              : source.OrderBy(selector);
        }

        public static IOrderedQueryable<TSource> OrderByDirection<TSource>(this IQueryable<TSource> source, string property, bool descending)
        {
            return descending ? source.OrderByDescending(GetLambda<TSource>(property))
                              : source.OrderBy(GetLambda<TSource>(property));
        }

        private static Expression<Func<TSource, object>> GetLambda<TSource>(string propertyName)
        {
            var param = Expression.Parameter(typeof(TSource), "p");
            var body = param.GetProperty(propertyName);
            return Expression.Lambda<Func<TSource, object>>(Expression.Convert(body, typeof(object)), param);
        }
    }
}
