﻿namespace GPNSM.Common.Core
{
    public static class ConstsCore
    {
        public static class RegularExpression
        {
            public static string Email = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        }
    }
}
