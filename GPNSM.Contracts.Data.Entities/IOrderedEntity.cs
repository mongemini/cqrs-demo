﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Contracts.Data.Entities
{
    public interface IOrderedEntity
    {
        DateTime CreatedDate { get; set; }
    }
}
