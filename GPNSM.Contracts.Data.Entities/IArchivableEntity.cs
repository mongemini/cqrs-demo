﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Contracts.Data.Entities
{
    public interface IArchivableEntity<TKey> : IAuditableEntity<TKey>
    {
        bool IsDeleted { get; set; }
    }

    public interface IArchivableEntity : IArchivableEntity<Guid>, IAuditableEntity { }
}
