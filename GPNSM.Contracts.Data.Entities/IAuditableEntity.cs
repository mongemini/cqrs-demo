﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Contracts.Data.Entities
{
    public interface IAuditableEntity<TKey> : IEntity<TKey>
    {
        DateTime CreatedDate { get; set; }
        TKey CreatedBy { get; set; }
        DateTime UpdatedDate { get; set; }
        TKey UpdatedBy { get; set; }
    }

    public interface IAuditableEntity : IAuditableEntity<Guid>, IEntity { }
}
