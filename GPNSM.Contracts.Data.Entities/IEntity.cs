﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Contracts.Data.Entities
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
    public interface IEntity : IEntity<Guid>, IOrderedEntity { }
}
