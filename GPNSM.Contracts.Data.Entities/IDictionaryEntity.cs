﻿namespace GPNSM.Contracts.Data.Entities
{
    public interface IDictionaryEntity : IEntity<int>
    {
        string Value { get; set; }
    }
}
