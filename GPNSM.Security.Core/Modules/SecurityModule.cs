﻿using Autofac;

namespace GPNSM.Security.Core.Modules
{
    public class SecurityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CurrentUserContextService>().AsSelf().AsImplementedInterfaces();
            base.Load(builder);
        }
    }
}
