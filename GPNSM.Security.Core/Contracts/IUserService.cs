﻿using GPNSM.Security.Core.Classes;
using System;
using System.Threading.Tasks;

namespace GPNSM.Security.Core.Contracts
{
    public interface IUserService
    {
        Task<Guid?> CreateUserAsync(CreateUserData userData);
    }
}
