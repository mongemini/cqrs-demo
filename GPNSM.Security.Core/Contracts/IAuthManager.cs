﻿using GPNSM.Security.Core.Classes;
using System.Threading.Tasks;

namespace GPNSM.Security.Core.Contracts
{
    public interface IAuthManager
    {
        Task<OidcTokenResponse> LoginUser(string username, string password);

        Task<bool> LogoutUser(string accessToken);

        Task<OidcUser> GetUserInfo(string accessToken);

        Task<OidcTokenResponse> Refresh(string accessToken);
    }
}
