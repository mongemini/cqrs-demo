﻿namespace GPNSM.Security.Core.Contracts
{
    public interface IRefreshTokenRepository
    {
        void AddRefreshToken(string asseccToken, string refreshToken);

        string GetRefreshToken(string asseccToken);

        void DeleteRefreshToken(string asseccToken);
    }
}
