﻿using System;

namespace GPNSM.Security.Core.Contracts
{
    public interface ICurrentUserService
    {
        Guid GetCurrentUserId();
    }
}
