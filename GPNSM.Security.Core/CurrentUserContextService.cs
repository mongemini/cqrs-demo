﻿using GPNSM.Security.Core.Contracts;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace GPNSM.Security.Core
{
    public class CurrentUserContextService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CurrentUserContextService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public Guid GetCurrentUserId()
        {
            if (Guid.TryParse(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out Guid userId))
                return userId;

            throw new UnauthorizedAccessException();
        }
    }
}
