﻿using Newtonsoft.Json;
using System;

namespace GPNSM.Security.Core.Classes
{
    public class OidcUser
    {
        [JsonProperty("sub")]
        public Guid Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("preferred_username")]
        public string FullName { get; set; }
    }
}
