﻿using Newtonsoft.Json;

namespace GPNSM.Security.Core.Classes
{
    public class OidcTokenResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }
        [JsonProperty("refresh_token")]
        public string RefeshToken { get; set; }
    }
}
