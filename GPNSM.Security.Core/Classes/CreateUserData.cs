﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.Security.Core.Classes
{
    public class CreateUserData
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int UserType { get; set; }
        public string JsonProperties { get; set; }
    }
}
