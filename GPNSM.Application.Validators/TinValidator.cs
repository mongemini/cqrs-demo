﻿using FluentValidation;
using GPNSM.Application.Validators.Resources;
using System.Linq;

namespace GPNSM.Application.Validators
{
    public class TinValidator : AbstractValidator<string>
    {
        public TinValidator()
        {
            RuleFor(value => value)
                .Length(10)
                .When(value => value?.Length != 12)
                .WithMessage(x => ValidationErrorMessage.TinValidatorLength);

            RuleFor(iec => iec)
                .Must(iec => iec.All(char.IsDigit))
                .When(iec => iec != null)
                .WithMessage(x => ValidationErrorMessage.TinValidatorDigit);

        }
    }
}
