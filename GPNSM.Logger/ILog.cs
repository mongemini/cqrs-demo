﻿using System;
using System.Threading.Tasks;

namespace GPNSM.Logger
{
    public interface ILog
    {
        void Info(string message);
        Task InfoAsync(string message);
        void Trace(string message);
        Task TraceAsync(string message);
        void Warning(string message, Exception exception = null);
        Task WarningAsync(string message, Exception exception = null);
        void Error(string message, Exception exception);
        Task ErrorAsync(string message, Exception exception);
    }
}
