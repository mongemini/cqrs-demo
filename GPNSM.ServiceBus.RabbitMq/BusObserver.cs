﻿using GPNSM.Logger;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace GPNSM.ServiceBus.RabbitMq
{
    public class BusObserver : IBusObserver
    {
        private readonly ILog _logger;
        public BusObserver(ILog logger)
        {
            _logger = logger;
        }

        public Task CreateFaulted(Exception exception)
        {
            return Task.Run(() => _logger.Error("Bus: CreateFaulted", exception));
        }

        public Task PostCreate(IBus bus)
        {
            return Task.Run(() => _logger.Trace("Bus: PostCreate"));
        }

        public Task PostStart(IBus bus, Task<BusReady> busReady)
        {
            return Task.Run(() => _logger.Trace("Bus: PostStart"));
        }

        public Task PostStop(IBus bus)
        {
            return Task.Run(() => _logger.Trace("Bus: PostStop"));
        }

        public Task PreStart(IBus bus)
        {
            return Task.Run(() => _logger.Trace("Bus: PreStart"));
        }

        public Task PreStop(IBus bus)
        {
            return Task.Run(() => _logger.Trace("Bus: PreStop"));
        }

        public Task StartFaulted(IBus bus, Exception exception)
        {
            return Task.Run(() => _logger.Error("Bus: StartFaulted", exception));
        }

        public Task StopFaulted(IBus bus, Exception exception)
        {
            return Task.Run(() => _logger.Error("Bus: StopFaulted", exception));
        }
    }
}
