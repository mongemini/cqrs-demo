﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPNSM.ServiceBus.RabbitMq
{
    public class RabbitMqOptions
    {
        public string Url { get; set; }
        public string QueueName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
