﻿using GPNSM.Logger;
using GreenPipes;
using System.Collections.Generic;
using System.Linq;

namespace GPNSM.ServiceBus.RabbitMq.LoggerFilter
{
    public class ExceptionLoggerSpecification<T> : IPipeSpecification<T>
        where T : class, PipeContext
    {
        private readonly ILog _logger;
        public ExceptionLoggerSpecification(ILog logger)
        {
            _logger = logger;
        }

        public IEnumerable<ValidationResult> Validate()
        {
            return Enumerable.Empty<ValidationResult>();
        }

        public void Apply(IPipeBuilder<T> builder)
        {
            builder.AddFilter(new ExceptionLoggerFilter<T>(_logger));
        }
    }
}
