﻿using GPNSM.Logger;
using GreenPipes;
using System;
using System.Threading.Tasks;

namespace GPNSM.ServiceBus.RabbitMq.LoggerFilter
{
    public class ExceptionLoggerFilter<T> : IFilter<T>
        where T : class, PipeContext
    {
        private readonly ILog _logger;
        public ExceptionLoggerFilter(ILog logger)
        {
            _logger = logger;
        }

        public void Probe(ProbeContext context)
        {
        }

        public async Task Send(T context, IPipe<T> next)
        {
            try
            {
                await next.Send(context);
            }
            catch (Exception ex)
            {
                _logger.Error("ExceptionLoggerFilter", ex);
                throw;
            }
        }
    }
}
