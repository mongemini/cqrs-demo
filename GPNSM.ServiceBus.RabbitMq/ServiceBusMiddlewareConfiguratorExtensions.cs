﻿using GPNSM.Logger;
using GPNSM.ServiceBus.RabbitMq.LoggerFilter;
using GreenPipes;

namespace GPNSM.ServiceBus.RabbitMq
{
    public static class ServiceBusMiddlewareConfiguratorExtensions
    {
        public static void UseExceptionLogger<T>(this IPipeConfigurator<T> configurator, ILog logger)
            where T : class, PipeContext
        {
            configurator.AddPipeSpecification(new ExceptionLoggerSpecification<T>(logger));
        }

    }
}
