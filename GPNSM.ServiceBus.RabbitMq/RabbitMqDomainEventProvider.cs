﻿using GPNSM.Domain.Core;
using MassTransit;
using System.Threading.Tasks;

namespace GPNSM.ServiceBus.RabbitMq
{
    public class RabbitMqDomainEventProvider: IExternalDomainEventProvider
    {
        private readonly IBusControl _serviceBus;

        public RabbitMqDomainEventProvider(IBusControl serviceBus)
        {
            _serviceBus = serviceBus;
        }

        public async Task PublishAsync<T>(T externalEvent) 
            where T: ExternalBaseDomainEvent
        {
            await _serviceBus.Publish(externalEvent);
        }
    }
}
