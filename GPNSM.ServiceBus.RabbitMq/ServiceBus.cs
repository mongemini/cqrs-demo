﻿using GPNSM.Logger;
using GPNSM.ServiceBus.Core.Contracts;
using MassTransit;
using MassTransit.RabbitMqTransport;
using System;
using System.Threading.Tasks;

namespace GPNSM.ServiceBus.RabbitMq
{
    public abstract class ServiceBus : IServiceBus
    {
        private readonly IBusConfiguration _busConfiguration;
        private readonly ILog _logger;
        protected readonly IServiceProvider _serviceProvider;
        private IBusControl _busControl;

        public ServiceBus(IBusConfiguration busConfiguration, ILog logger, IServiceProvider serviceProvider)
        {
            _busConfiguration = busConfiguration;
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public async Task StartAsync()
        {
            _busControl = ConfigureBus();
            await _busControl.StartAsync();
        }

        public async Task StopAsync()
        {
            await _busControl?.StopAsync(TimeSpan.FromSeconds(30));
        }

        public void Send<T>(T message) where T : class
        {
            if (_busControl != null)
            {
                _busControl.Send(message);
            }
        }

        public void Publish<T>(T message) where T : class
        {
            if (_busControl != null)
            {
                _busControl.Publish(message);
            }
        }

        public void Publish<T>(T message, Action<PublishContext<T>> contextCallback) where T : class
        {
            if (_busControl != null)
            {
                _busControl.Publish(message, contextCallback);
            }
        }

        private IBusControl ConfigureBus()
        {
            var logger = _serviceProvider.GetService(typeof(ILog)) as ILog;
            var busObserver = new BusObserver(logger);

            var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri(_busConfiguration.Url), h =>
                {
                    h.Username(_busConfiguration.UserName);
                    h.Password(_busConfiguration.Password);
                });

                cfg.UseExceptionLogger(logger);
                cfg.ReceiveEndpoint(host, e =>
                {
                    ConfigureConsumer(e);
                });
                cfg.BusObserver(busObserver);
            });

            return busControl;
        }

        protected virtual void ConfigureConsumer(IRabbitMqReceiveEndpointConfigurator configuration) { }
    }
}
